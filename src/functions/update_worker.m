function update_worker()

%% changelog
%
% v0.1 - 2017-08-14 - F Schweser

versionNo = 0.1;
disp(['Updater v.' num2str(versionNo)])



global studyBaseFolder

%% Update study

shellScriptCode = fileread(fullfile(studyBaseFolder,'instance.conf'));
eval(shellScriptCode)

if isDebug == true
    addpath(genpath(fullfile(baseBrukerSkyLocation,'system/src')))
else
    addpath(genpath(fullfile(studyBaseFolder,foldernameSystem)))
end


global systemIncludesLocation

shellScriptCode = fileread(fullfile(studyBaseFolder,'instance.conf'));
eval(shellScriptCode)

shellScriptCode = fileread(fullfile(baseBrukerSkyLocation,'system','global.conf'));
eval(shellScriptCode)

configurationFolder             = fullfile(studyBaseFolder,foldernameConfigs);
globalDatabaseFile_relative     = fullfile(foldernameGlobalDatabase,globalDBfilename);
databaseFile                    = fullfile(baseBrukerSkyLocation,globalDatabaseFile_relative);
dataStorage                     = fullfile(baseBrukerSkyLocation,foldernameOriginalData);
systemIncludesLocation          = fullfile(baseBrukerSkyLocation,foldernameIncludes);

% Instance-related locations
recoStorage                     = fullfile(studyBaseFolder,foldernameRecoData);
shellScriptFolder               = fullfile(studyBaseFolder,foldernameRecoScripts); 
recoModulesFolder               = fullfile(studyBaseFolder,foldernameRecoModules); 
recoFunctionsFolder             = fullfile(studyBaseFolder,foldernameRecoFunctions); 
confFileSequences               = fullfile(configurationFolder,confFileSequences); %#ok<NODEF>
confFileRecos                   = fullfile(configurationFolder,confFileRecos); %#ok<NODEF>
confFileStudyName               = fullfile(configurationFolder,confFileStudyName); %#ok<NODEF>
confFileAnimalID               = fullfile(configurationFolder,confFileAnimalID); %#ok<NODEF>
studyDBfile                     = fullfile(studyBaseFolder,studyDBfilename);
recoDBfile                      = fullfile(studyBaseFolder,recoDBfilename);
shellScriptFolder               = fullfile(studyBaseFolder,foldernameRecoScripts);




%% Initialize instance
%addpath /shared/studies/nonregulated/Bruker_studies/db_repository/schweserbox/common_matlab/initialization/autocomp/
%initializeswitoolbox('buffalo')

initializesendmail(emailSendmail,environment_email);

if ~exist(shellScriptFolder,'dir')
    mkdir(shellScriptFolder)
end

db = brukerDB(databaseFile); % initialize global DB

% Load basic run information for this instance

isForcedToFirstTimeMode = false;
if ~exist(studyDBfile,'file')
    operationMode = 'firstTime'; 
    isForcedToFirstTimeMode = true;
end


%% Update StudyDB

% Find exams belonging to this study
if exist(confFileStudyName,'file')
    fileID = fopen(confFileStudyName,'r');
    dataArray = textscan(fileID, '%s%[^\n\r]', 'Delimiter', '',  'ReturnOnError', false);
    fclose(fileID);
    confstudyName = [dataArray{1:end-1}];
    clearvars fileID dataArray
    examIDs = db.find(confstudyName{1},'studyName',true);
    for jStudyName = 2:size(confstudyName,1)
        examIDs = [examIDs; db.find(confstudyName{jStudyName},'studyName',true)];
    end
else
    examIDs = [];
end

if exist(confFileAnimalID,'file')
    fileID = fopen(confFileAnimalID,'r');
    dataArray = textscan(fileID, '%s%[^\n\r]', 'Delimiter', '',  'ReturnOnError', false);
    fclose(fileID);
    confAnimalID = [dataArray{1:end-1}];
    clearvars fileID dataArray
    for jAnimalID = 1:size(confAnimalID,1)
        examIDs = [examIDs; db.find(confAnimalID{jAnimalID},'animalID',true)];
    end
end
nExamIDs = numel(examIDs);


% update study DB


fileID = fopen(confFileRecos,'r');
recosDefinition = textscan(fileID, '%s%s%s%[^\n\r]', 'Delimiter', ';',  'ReturnOnError', false);
fclose(fileID);
recosDefinition = [recosDefinition{1:end-1}];
clear fileID

if exist(confFileSequences,'file')
    relevantScans = unique(recosDefinition(:,1));
    fileID = fopen(confFileSequences,'r');
    sequenceDetails = textscan(fileID, '%s%s%s%s%s%[^\n\r]', 'Delimiter', ';',  'ReturnOnError', false);
    fclose(fileID);

    sequenceDetails = [sequenceDetails{1:end-1}];
    clearvars fileID
    expected = cell(1,size(relevantScans,1));
    for jScan = 1:size(relevantScans,1)
        expected{jScan}.ACQ_scan_name        = relevantScans{jScan,1};

        [sequenceDetailsFound,~] = find(ismember(sequenceDetails,expected{jScan}.ACQ_scan_name));

        if isempty(sequenceDetailsFound)
            error('Please make sure each entry in the recos.def matches an entry in the sequences.opt')
        end
        
        if ~isempty(sequenceDetails{sequenceDetailsFound,2})
            expected{jScan}.chooseMultiple = sequenceDetails{sequenceDetailsFound,2};
        else
            expected{jScan}.chooseMultiple = 'last'; % default
        end
        if ~isempty(sequenceDetails{sequenceDetailsFound,3})
            expected{jScan}.raw.fileSize = str2double(sequenceDetails{sequenceDetailsFound,3});
        end
        if ~isempty(sequenceDetails{sequenceDetailsFound,4})
            expected{jScan}.raw.fileList  = strsplit(sequenceDetails{sequenceDetailsFound,4},',');
        end
        if ~isempty(sequenceDetails{sequenceDetailsFound,5})
            expected{jScan}.reco.fileList  = strsplit(sequenceDetails{sequenceDetailsFound,5},',');
        end
    end
end

study = studyDB(studyDBfile);
%eval(conf_sequences);

switch operationMode
    case 'firstTime'
        study.define(expected);
    case 'applyStudyChanges'
        study.change(expected,dataStorage,db); 
end

if isempty(examIDs)
    dispstatus('No exams found for this study.')
else
    study.add(db,examIDs,dataStorage);
    study.save;
    
    studyInstanceContentTable=study.show;
    writetable(studyInstanceContentTable,fullfile(recoStorage,['databaseSnapshot_' datestr(now,'YYmmDD_HHMMSS') '.csv']))
end


% update reconstruction DB
recoDef = cell(size(recosDefinition,1)+1,1);
recoDef{1} = studyDBfile;
for jReco = 1:size(recosDefinition,1)
    recoDef{jReco+1}.ACQ_scan_name        = recosDefinition{jReco,1};
    recoDef{jReco+1}.recoFct              = fullfile(recoModulesFolder,recosDefinition{jReco,2},[recosDefinition{jReco,3} '.m']);
    recoDef{jReco+1}.reqRamGB             = 4; % not implemented yet
    recoDef{jReco+1}.reqCpuNo             = 1;% not implemented yet
    fopen_filename = fullfile(recoModulesFolder,recosDefinition{jReco,2},moduleDependenciesFile);
    fileID = fopen(fopen_filename,'r');
    if fileID == -1
        error(['File ' fopen_filename ' does not exist.'])
    end
    moduleDep = textscan(fileID, '%s%s%[^\n\r]', 'Delimiter', ';',  'ReturnOnError', false);
    fclose(fileID);
    moduleDep = [moduleDep{1:end-1}];
    [dependencyFound,~] = find(ismember(moduleDep,recosDefinition{jReco,3}));
    if ~isempty(dependencyFound)
        recoDef{jReco+1}.waitForResultFile    = moduleDep{dependencyFound,2};
    end
end

switch operationMode
    case {'applyStudyChanges','routine','callerFctDevelopment'}
        if ~isempty(examIDs)
  
            reco = recoDB(recoDBfile,recoStorage);

            oldPath = pwd;
            cd(shellScriptFolder) % all reco-functions assume that shell scripts are in current folder

            reco.update(recoDef,1); % checks again
            %reco.update(recoDef,2); % checks all completed recos
            %reco.update(recoDef,3,5); % forces new reconstruction of reco #3
            %reco.update(recoDef,3,6); % forces new reconstruction of reco #3
            %reco.update(recoDef,3,7); % forces new reconstruction of reco #3


            reco.createRecoScripts(databaseFile,dataStorage,recoScriptGenerationRetries);
            
            if ~strcmp(operationMode,'callerFctDevelopment')
                reco.cleanup;
                reco.update(recoDef)
                %reco.showstatus
                reco.save;
                cd (oldPath)
            end
        end
    otherwise
        reco = [];
end
disp('**********************')
disp(['Execution has finished for update script in ' studyBaseFolder])
disp('Summary:')
disp(['Total number of exams in this study: ' num2str(nExamIDs)])

if isForcedToFirstTimeMode
    disp('Mode was forced to "firstTime" because studyDB did not exist. Run this script again to create reconstruction scripts.')
end


end







