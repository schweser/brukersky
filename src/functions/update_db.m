function update_db(confFile,toolboxpath)

    %clear
    %restoredefaultpath


    %confFile      = '/shared/studies/nonregulated/Bruker_studies/db/conf_updateDB.conf';
    %addpath /shared/studies/nonregulated/Bruker_studies/db_repository/schweserbox/common_matlab/initialization/autocomp/
    addpath(toolboxpath)
    initializeswitoolbox('buffalo')
    %initializesendmail('ctrc-mri@bnac.net','bnac');

    % the following does not work within docker container
    %try
    %    exec_system_command('sudo -v');
    %catch
    %    error('This script can only be run by sudoer.')
    %end


    %% load configuration file

    conf_global     = read_configuration('global',confFile);
    eval(conf_global);




    %% update global DB
    db = brukerDB(databaseFile);
    db.scan(dataStorage,1);
    db.save;

end
