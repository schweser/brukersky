function [errorCode,shellOutput] = exec_system_command(commandStr,Options)

%EXEC_SYSTEM_COMMAND Wrapper for safe execution of shell commands.
%
%   Syntax
%
%   EXEC_SYSTEM_COMMAND(str)
%   EXEC_SYSTEM_COMMAND(str,Options)
%   [errorCode,shellOutput] = EXEC_SYSTEM_COMMAND(...);
%
%
%   Description
%
%   EXEC_SYSTEM_COMMAND(str) executes the string str. If error code is
%   unequal 0 an error is thrown.
%
%   EXEC_SYSTEM_COMMAND(str,Options) uses the Options-struct with the
%   following possible fields:
%
%       isForceOutput   - boolean. Forces shell output to be written to the
%       command line. (default: false)
%
%       noErrorOnShellErrorCode   - boolean. Tolerates shell error status codes.
%                                   This may be used if the shell status code
%                                   is erroneously set to error although no
%                                   error occured. (default: false)
%       noShellCommandPrint   -  boolean. Prevents output of the shell
%                                   command. (default: false)

%   F Schweser - 2013/11/25 
%
%   Changelog:
%
%   v1.0 - 2013/11/25 - F Schweser - Initial version.OptionsExec
%   v1.1 - 2013/12/03 - F Schweser - Modification for block of SYSTEM()
%   v1.2 - 2013/12/04 - F Schweser - New feature;: noErrorOnShellErrorCode
%   v1.3 - 2013/12/04 - F Schweser - New feature: noShellCommandPrint

if nargin < 2 || isempty(Options)
    Options.dummy = [];
end

if isfield(Options,'isForceOutput')
    outputForceStr = '-echo';
else
    outputForceStr = '';
end

if ~isfield(Options,'noErrorOnShellErrorCode') || isempty(Options.noErrorOnShellErrorCode)
    Options.noErrorOnShellErrorCode = false;
end
if ~isfield(Options,'noShellCommandPrint') || isempty(Options.noShellCommandPrint)
    Options.noShellCommandPrint = false;
end

if ~Options.noShellCommandPrint
    dispstatus(commandStr,Options,'unix');
end

%Options.noErrorOnShellErrorCode = true;

%systemReplacementFilePath = fileparts(which('system.m'));
%rmpath(systemReplacementFilePath)    
%[errorCode,shellOutput] = system(commandStr,outputForceStr);
[errorCode,shellOutput] = system(commandStr);
%warning('off')
%addpath(systemReplacementFilePath)
%warning('on')

%currDir = pwd;
%cd('/')
if errorCode && ~Options.noErrorOnShellErrorCode
    error(['Execution of shell command failed with error code ' num2str(errorCode) '. Shell output was: ' shellOutput ' This error may result from non-executable rights on the binary. Please do chmod a+x BINFILENAME']);
elseif Options.noErrorOnShellErrorCode
    dispstatus(shellOutput,Options,'warning');    
end
%cd(currDir)

end