function initializesendmail(mail_address,environment)

%INITIALIZESENDMAIL Initializes sendmail.
%
%
%   This snipplet initializes sendmail settings. Once this script
%   has been called mails can be sent from matlab using the SENDMAIL call.
%
%
%   Syntax
% 
%   INITIALIZESENDMAIL(senderAddStr,environment) 
%
%
%   Description
%
%   INITIALIZESENDMAIL(senderAddStr,environment) initializes sendmail for the sender
%   adress given by the string senderAddStr.
%
%
%
%   After calling this script send a mail:
%   sendmail('christian.ros@med.uni-jena.de', 'Wir machen es anders', ...
%    'Ich bestelle und hole auch alles ab. Bis die Mensa oben wieder oeffnet!!!! FS')
%
%
%   See also: SENDMAIL

%   2012/03/20 F Schweser, mail@ferdinand-schweser.de
%
%   Changelog:
%   2015/05/06 - v1.1 - F Schweser - added environment


switch environment
    case 'jena'
        
        % Define these variables appropriately:
        user_name = '';                          % My Username
        password = '';      % My E-Mail Password
        smtp_server = 'ukjmailgw.med.uni-jena.de';
        
        % Then this code will set up the preferences properly:
        setpref('Internet', 'E_mail', mail_address);
        setpref('Internet', 'SMTP_Username', user_name);
        setpref('Internet', 'SMTP_Password', password);
        setpref('Internet', 'SMTP_Server', smtp_server);
        props = java.lang.System.getProperties;
        props.setProperty('mail.smtp.auth', 'false');
        %props.setProperty('mail.smtp.socketFactory.class', 'javax.net.ssl.SSLSocketFactory');
        %props.setProperty('mail.smtp.socketFactory.port', '465');
        
    case 'bnac'
        % Define these variables appropriately:
        user_name = 'ctrc-mri@bnac.net';                          % My Username
        password = 'uGwk5e0e9G(v<phT';      % My E-Mail Password
        smtp_server = 'smtp.gmail.com';
        
        % Then this code will set up the preferences properly:
        setpref('Internet', 'E_mail', mail_address);
        setpref('Internet', 'SMTP_Username', user_name);
        setpref('Internet', 'SMTP_Password', password);
        setpref('Internet', 'SMTP_Server', smtp_server);
        props = java.lang.System.getProperties;
        props.setProperty('mail.smtp.auth', 'true');
        props.setProperty('mail.smtp.socketFactory.class', 'javax.net.ssl.SSLSocketFactory');
        props.setProperty('mail.smtp.socketFactory.port', '465');
    otherwise
        error('Environment not supported.')

end
