function update()

%%% This is the central update script for the instance.
%
% This script must be located in the instance's base folder.
%
% Usually no changes are required in this script. It may be used for all
% instances as long as the general file system hierarchy is maintained.
%
% Changes in the general file system hierarchy require changes of the
% variables in this file.


%% Reset MATLAB workspace
munlock
clear
clear functions
clear brukerDB recoDB studyDB brukerData
restoredefaultpath

global studyBaseFolder
studyBaseFolder = fileparts(mfilename('fullpath'));

shellScriptCode = fileread(fullfile(studyBaseFolder,'instance.conf'));
eval(shellScriptCode)

if isDebug == true
    addpath(genpath(fullfile(baseBrukerSkyLocation,'system/source')))
else
    addpath(genpath(fullfile(studyBaseFolder,foldernameSystem)))
end

update_worker();

end







