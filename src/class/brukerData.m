classdef brukerData
    
    % internal class for storing meta data for bruker data folders.
    % The following methods are available:
    %
    %   (constructor)  - creates a new instance based on a provided subject
    %                    file
    %   .check         - consistency check for the instance
    %   .exist         - checks if the subject file of the instance still
    %                    exists.
    %
    % The following properties exist:
    %
    %   studyName      - "Subject name" as specified in ParaVision
    %   animalID       - "Registration" specified in PV
    %   examName       - "Study" specified in PV 
    %   instanceUID    - Unique identifier of registration
    %   location       - location of dataset on local file system

 % F Schweser - 30/04/2015 
 %
 % Changelog
 %
 %  30/04/2015 - v1.0 - F Schweser - initial
 %  16/11/2015 - v1.1 - F Schweser - added instanceUID
 %  15/06/2016 - v1.2 - F Schweser - Now works with PV6
    properties (Hidden = false, GetAccess = public, SetAccess = private)
        studyName
        animalID
        instanceUID
        examName
        location
    end
    
    methods
        
        
        %% get methods
        function obj = brukerData(dataFolder,dataStorageFolder)
            % class constructor
            if nargin < 2 || isempty(dataStorageFolder)
                dataStorageFolder = '';
            end
            if exist(fullfile(dataStorageFolder,dataFolder,'subject'),'file')
                try
                    ParamFile_subject = readBrukerParamFile(fullfile(dataStorageFolder,dataFolder,'subject'));
                    obj.studyName = ParamFile_subject.SUBJECT_name_string;
                    obj.animalID = ParamFile_subject.SUBJECT_id;
                    if myisfield(ParamFile_subject,'SUBJECT_patient_instance_uid')
                        obj.instanceUID = ParamFile_subject.SUBJECT_patient_instance_uid;
                    else
                        obj.instanceUID = ParamFile_subject.SUBJECT_instance_uid;
                    end
                    obj.location = dataFolder;
                catch
                    error(['Cannot read study file ' fullfile(dataStorageFolder,dataFolder,'subject')])
                end
                
                if isfield(ParamFile_subject,'SUBJECT_study_name')
                    obj.examName = ParamFile_subject.SUBJECT_study_name;
                else
                    obj.examName = [];
                    dispstatus('No study defined in this dataset. Does not contain data.')
                end
                
            else
                error(['Study file does not exist: ' fullfile(dataStorageFolder,dataFolder,'subject')])
            end
        end

        function value = check(obj,dataStorageFolder)
            value = true;
            if ~ischar(obj.studyName) || isempty(obj.studyName) || ...
                    ~ischar(obj.animalID) || isempty(obj.animalID) || ...
                    ~ischar(obj.location) || isempty(obj.location) || ...
                    ~ischar(obj.instanceUID) || isempty(obj.instanceUID) || ...
                    ~ischar(obj.examName) || isempty(obj.examName) || ...
                    ~exist(fullfile(dataStorageFolder,obj.location),'dir')
                value = false;
            end
        end
        
        function isExist = exist(obj,dataStorageFolder)
            isExist = false;
            if exist(obj.location,'dir')
                if exist(fullfile(dataStorageFolder,obj.location,'subject'),'file')
                    isExist = true;
                end
            end
        end
        
        
    end
end
