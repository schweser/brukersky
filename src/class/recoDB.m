classdef recoDB < handle
    
    % database class for reconstructions. 
    %
    % The following public methods are available:
    %
    %   (constructor)       - loads/creates a (new) database file (mat-file to
    %                         be saved) based in the specified file name.
    %   .save               - saves changes made to database to the
    %                         database file
    %   .update             - This member updates the whole database.
    %                         Please note this is the only way to perform
    %                         an update. E.g. calling showstatus multiple
    %                         times will anot provide updated information
    %                         about the job status, if update is not called
    %                         in between. Update checks all reconstruction
    %                         folders and shell script files for
    %                         completeness.
    %   .createRecoScripts  - Creates the reconstruction shell scripts by
    %                         calling the reco caller functions.
    %   .remove             - removes a reconstruction
    %   .showstatus         - Lists all runnin, completed and failed jobs
    %   .cleanup            - deletes error information and cleans up old
    %                         shell scripts
    %   .recoError          - provides detailed error information
    
    
 % F Schweser - 7/05/2015 
 %
 % Changelog
 %
 %  7/05/2015 - v1.0 - F Schweser - initial
 %  23/05/2015 - v1.1 - F Schweser - bugfix add
 %  15/06/2016 - v1.2 - F Schweser - bugfix update function when db empty
    
    properties (Hidden = true, GetAccess = private, SetAccess = private)
        databaseRoot
        definition
        brukerDB_id
    end
    
    properties (Hidden = false, GetAccess = public, SetAccess = private)
        dbLocation
        status
        lastSaved
        resultsBaseFolder
    end
    
    properties (Constant)
        dbVersion = 'v1.0';
    end
    
    methods
        
        %% get methods
        function obj = recoDB(dbLocation,resultsBaseFolder)
            % class constructor
            
            obj.load(dbLocation);
            obj.resultsBaseFolder = resultsBaseFolder;
            if ~exist(obj.resultsBaseFolder,'dir')
                mkdir(obj.resultsBaseFolder);
            end
        end
        
        function obj = save(obj)
            dbVar = obj.databaseRoot; %#ok<NASGU>
            dbID = obj.brukerDB_id; %#ok<NASGU>
            DB_VERSION = obj.dbVersion; %#ok<NASGU>
            obj.lastSaved = datestr(clock,0);
            save(obj.dbLocation,'dbVar','DB_VERSION','dbID');
            obj.status = 'saved';
        end
        
        
        
         function update(obj,definitionCell,forceLevel,recoDefNumbers) 
             % forceLevel = 0 (default) adds only new exams in the study
             % db.
             % forceLevel = 1 checks all scans in the study db for new
             % reconstructions, but does not force reconstruction, if
             % exists
             % forceLevel = 2 checks all complteted recos for completeness
             % forceLevel = 3 forces a recomputation of all
             % reconstructions. Using recoDefNumber allows you to restrict
             % recomputation to a certain reconstruction. recoDefNumber is
             % the number of the reconstruction in the configuration file.
             % The class compared sequence type and caller function. If
             % they match, the reconstruction will be reset.
             
             if nargin < 3 || isempty(forceLevel)
                 forceLevel = 0;
             end
             
             studyDbInstance = studyDB(definitionCell{1});
             if isempty(obj.brukerDB_id)
                 obj.brukerDB_id = studyDbInstance.brukerDbID;
             end
             
             % check for new entries in study DB
             if forceLevel ~= 1
                 newExamIDs = obj.findNewExams(studyDbInstance);
                 % update internal database with new study scans
             else
                 highestExamID = studyDbInstance.show('last').examID; 
                 newExamIDs = 1:highestExamID;
             end
             obj.add(studyDbInstance,newExamIDs,definitionCell);
             
             if forceLevel == 3
                 if nargin > 3
                    obj.checkReco(true,[],definitionCell,recoDefNumbers);
                 else
                     obj.checkReco(true);
                 end
             elseif forceLevel == 2
                obj.checkReco([],[],[],[],true);
             else
                obj.checkReco([],true);
             end
             
             % send emails about failed scans
             %obj.sendErrorEmails('fschweser@bnac.net',4);
             
             
         end
         
         
         function createRecoScripts(obj,brukerDBFile,dataStoragePath,nErrorThreshold)
             
             if nargin < 2
                 nErrorThreshold = 2;
             end
             
             brukerDbInstance = brukerDB(brukerDBFile);
             
             
             for jBatch = 1:3
                 if jBatch == 1
                    recosNew = obj.show('waiting');
                 elseif jBatch == 2
                    recosNew = obj.show('new');
                 else
                    recosNew = obj.show('failed');
                 end

                 if ~isempty(recosNew)
                     for jExamID = recosNew.examIDs
                         recoIDs = 1:numel(recosNew.details{find(recosNew.examIDs == jExamID)}); %#ok<FNDSB>
                         recoIDs = recoIDs(recosNew.details{find(recosNew.examIDs == jExamID)}); %#ok<FNDSB>
                         for jReco = recoIDs 

                             resultFolder = obj.genResultFolder(jExamID,brukerDbInstance);

                             waitingForFile = false;
                             if ~isempty(obj.databaseRoot{jExamID}{jReco,4}.waitForResultFile) && ~exist(fullfile(resultFolder,obj.databaseRoot{jExamID}{jReco,4}.waitForResultFile),'file')
                                 waitingForFile = true;
                                 obj.databaseRoot{jExamID}{jReco,4}.invoked = now; % in this case "invoked" just means that we tried to run it, but file was not yet there.
                             end

                            if ~waitingForFile && (isempty(obj.recoError(jExamID,jReco)) || numel(obj.recoError(jExamID,jReco)) < nErrorThreshold) % no errors or number of errors below threshold


                                [matFctPath,matFctName] = fileparts(fullfile(obj.databaseRoot{jExamID}{jReco,3}));

                                addpath(matFctPath)
                                if exist(matFctName) ~= 2 %#ok<EXIST>
                                    error(['Matlab function ' obj.databaseRoot{jExamID}{jReco,3} ' not found.']);
                                end
                                fHandle = str2func(matFctName);

                                scanFolder = fullfile(dataStoragePath,obj.databaseRoot{jExamID}{jReco,2});

                                scriptFileName = ['SU' brukerDbInstance.show(jExamID).animalID '_EX' num2str(jExamID,'%06.0f') '_RE' num2str(jReco,'%06.0f') '_' matFctName '_' datestr(now,'yyyymmdd_HHMMSS') '.sh'];
                                requirements.ram = obj.databaseRoot{jExamID}{jReco,4}.reqRamGB;
                                requirements.cpu = obj.databaseRoot{jExamID}{jReco,4}.reqCPU;
                                requirements.reconstructionNo = jReco;

                                try
                                    [resultFiles, maxRunTime] = fHandle(scanFolder,scriptFileName,resultFolder,requirements);
                                catch exceptionStr
                                    error(['There is a problem with the caller function ' matFctName ': ' exceptionStr.message])
                                end
                                rmpath(matFctPath)

                                if ~exist(scriptFileName,'file')
                                    error(['Matlab function ' obj.databaseRoot{jExamID}{jReco,3} ' did not create shell script.'])
                                end

                                if ~iscell(resultFiles) || ~isnumeric(maxRunTime)
                                    if exist(scriptFileName,'file')
                                        exec_system_command(['rm ' scriptFileName]);
                                    end
                                    error(['Reconstruction ' obj.databaseRoot{jExamID}{jReco,3} ' does not return proper variables. (Script has been deleted before this message was thrown.)'])
                                end

                                obj.databaseRoot{jExamID}{jReco,4}.resultFiles = cellfun(@(sas) fullfile(resultFolder,sas), resultFiles, 'uni', false);
                                obj.databaseRoot{jExamID}{jReco,4}.script = scriptFileName;
                                obj.databaseRoot{jExamID}{jReco,4}.maxRunTime = maxRunTime;
                                obj.databaseRoot{jExamID}{jReco,4}.invoked = now;
                                obj.databaseRoot{jExamID}{jReco,4}.complete = false;
                                obj.databaseRoot{jExamID}{jReco,4}.running = true;
                                obj.databaseRoot{jExamID}{jReco,4}.waitForResultFile = [];
                                obj.save;
                            elseif numel(obj.recoError(jExamID,jReco)) >= nErrorThreshold
                                %sendmail('fschweser@bnac.net', ['[BrukerSky] Too many failed reconstruction attempts. EX' num2str(jExamID) '; Sequence ' obj.databaseRoot{jExamID}{jReco,1}], ...
                                %    ['Caller function: ' obj.databaseRoot{jExamID}{jReco,3}]);
                                % sendmail currently not supported
                            end
                         end
                     end
                 end
             end
             
         end
         
        
        function remove(obj,examID)
            if examID <= size(obj.databaseRoot,1)
                for jElement = 1:size(obj.databaseRoot,2)
                    obj.databaseRoot{examID,jElement} = [];
                    obj.status = 'modified';
                end
            end
        end
        
        function showstatus(obj)
            
            
            newExamIDs = obj.show('new');
            jCounter = 0;
            if ~isempty(newExamIDs)
                for jExamID = 1:size(newExamIDs.examIDs,2)
                    for jReco = 1:numel(newExamIDs.details{jExamID})
                        if newExamIDs.details{jExamID}(jReco)
                            jCounter = jCounter + 1;
                            cellNew.exID{jCounter}           = newExamIDs.examIDs(jExamID);
                            cellNew.ACQ_scan_name{jCounter}  = obj.databaseRoot{newExamIDs.examIDs(jExamID)}{jReco,1};
                            [~,recoFunctionName,recoFunctionext] = fileparts(obj.databaseRoot{newExamIDs.examIDs(jExamID)}{jReco,3});
                            cellNew.recoFct{jCounter}        = [recoFunctionName,recoFunctionext];
                        end
                    end
                end
            end
            disp('===============================================================')
            disp('New jobs (ready, but shell scripts not yet created): ')
            if jCounter
                disp(table(cell2mat(cellNew.exID)',cellNew.recoFct',cellNew.ACQ_scan_name','VariableNames',{'examID','recoFct','scanName'}))
            else
                disp('(NONE)');
            end
            
            waitingExamIDs = obj.show('waiting');
            jCounter = 0;
            if ~isempty(waitingExamIDs)
                for jExamID = 1:size(waitingExamIDs.examIDs,2)
                    for jReco = 1:numel(waitingExamIDs.details{jExamID})
                        if waitingExamIDs.details{jExamID}(jReco)
                            jCounter = jCounter + 1;
                            cellWaiting.exID{jCounter}           = waitingExamIDs.examIDs(jExamID);
                            cellWaiting.ACQ_scan_name{jCounter}  = obj.databaseRoot{waitingExamIDs.examIDs(jExamID)}{jReco,1};
                            [~,recoFunctionName,recoFunctionext] = fileparts(obj.databaseRoot{waitingExamIDs.examIDs(jExamID)}{jReco,3});
                            cellWaiting.recoFct{jCounter}        = [recoFunctionName,recoFunctionext];
                            cellWaiting.waitingFor{jCounter}     = obj.databaseRoot{waitingExamIDs.examIDs(jExamID)}{jReco,4}.waitForResultFile;
                        end
                    end
                end
            end
            disp('===============================================================')
            disp('Waiting jobs (Waiting for the creation of a file in the results folder that was not present when shell scripts were created last time): ')
            if jCounter
                disp(table(cell2mat(cellWaiting.exID)',cellWaiting.recoFct',cellWaiting.ACQ_scan_name',cellWaiting.waitingFor','VariableNames',{'examID','recoFct','scanName','waitingFor'}))
            else
                disp('(NONE)');
            end
            
            runningExamIDs = obj.show('running');
            jCounter = 0;
            if ~isempty(runningExamIDs)
                for jExamID = 1:size(runningExamIDs.examIDs,2)
                    for jReco = 1:numel(runningExamIDs.details{jExamID})
                        if runningExamIDs.details{jExamID}(jReco)
                            jCounter = jCounter + 1;
                            cellComplete.exID{jCounter}           = runningExamIDs.examIDs(jExamID);
                            cellComplete.ACQ_scan_name{jCounter}  = obj.databaseRoot{runningExamIDs.examIDs(jExamID)}{jReco,1};
                            [~,recoFunctionName,recoFunctionext] = fileparts(obj.databaseRoot{runningExamIDs.examIDs(jExamID)}{jReco,3});
                            cellComplete.recoFct{jCounter}        = [recoFunctionName,recoFunctionext];
                            cellComplete.invoked{jCounter}        = datestr(obj.databaseRoot{runningExamIDs.examIDs(jExamID)}{jReco,4}.invoked);
                            cellComplete.targetfolder{jCounter}   = fileparts(obj.databaseRoot{runningExamIDs.examIDs(jExamID)}{jReco,4}.resultFiles{1});

                        end
                    end
                end
            end
            disp('===============================================================')
            disp('Queued or running jobs (shell scripts created): ')
            if jCounter
                disp(table(cell2mat(cellComplete.exID)',cellComplete.recoFct',cellComplete.ACQ_scan_name',cellComplete.invoked',cellComplete.targetfolder','VariableNames',{'examID','recoFct','scanName','created','targetFolder'}))
            else
                disp('(NONE)');
            end
            
            cellComplete = [];
            completeExamIDs = obj.show('complete');
            jCounter = 0;
            if ~isempty(completeExamIDs)
                for jExamID = 1:size(completeExamIDs.examIDs,2)
                    for jReco = 1:numel(completeExamIDs.details{jExamID})
                        if completeExamIDs.details{jExamID}(jReco)
                            jCounter = jCounter + 1;
                            cellComplete.exID{jCounter}           = completeExamIDs.examIDs(jExamID);
                            cellComplete.ACQ_scan_name{jCounter}  = obj.databaseRoot{completeExamIDs.examIDs(jExamID)}{jReco,1};
                            [~,recoFunctionName,recoFunctionext] = fileparts(obj.databaseRoot{completeExamIDs.examIDs(jExamID)}{jReco,3});
                            cellComplete.recoFct{jCounter}        = [recoFunctionName,recoFunctionext];
                            if isempty(obj.databaseRoot{completeExamIDs.examIDs(jExamID)}{jReco,4}.resultFiles)
                                cellComplete.targetfolder{jCounter}   = 'n/a';
                            else
                                cellComplete.targetfolder{jCounter}   = fileparts(obj.databaseRoot{completeExamIDs.examIDs(jExamID)}{jReco,4}.resultFiles{1});
                            end
                            cellComplete.invoked{jCounter}        = datestr(obj.databaseRoot{completeExamIDs.examIDs(jExamID)}{jReco,4}.invoked,'yyyy-mm-dd HH:MM');

                        end
                    end
                end
            end
            disp('===============================================================')
            disp('Completed jobs: ')
            if jCounter
                disp(sortrows(table(cell2mat(cellComplete.exID)',cellComplete.invoked',cellComplete.recoFct',cellComplete.ACQ_scan_name',cellComplete.targetfolder','VariableNames',{'examID','invoked','recoFct','scanName','resultsFolder'}),{'examID'}))
            else
                disp('(NONE)');
            end
            
            
            failedExamIDs = obj.show('failed');
            if ~isempty(failedExamIDs)
                jCounter = 0;
                for jExamID = 1:size(failedExamIDs.examIDs,2)
                    for jReco = 1:numel(failedExamIDs.details{jExamID})
                        if failedExamIDs.details{jExamID}(jReco)
                            jCounter = jCounter + 1;
                            cellFailed.exID{jCounter}           = failedExamIDs.examIDs(jExamID);
                            cellFailed.invoked{jCounter}        = datestr(obj.databaseRoot{failedExamIDs.examIDs(jExamID)}{jReco,4}.invoked,'yyyy-mm-dd HH:MM');

                            cellFailed.ACQ_scan_name{jCounter}  = obj.databaseRoot{failedExamIDs.examIDs(jExamID)}{jReco,1};
                            [~,recoFunctionName,recoFunctionext] = fileparts(obj.databaseRoot{failedExamIDs.examIDs(jExamID)}{jReco,3});
                            cellFailed.recoFct{jCounter}        = [recoFunctionName,recoFunctionext];
                            cellFailed.errorDate{jCounter}        = obj.databaseRoot{failedExamIDs.examIDs(jExamID)}{jReco,4}.error{end}.identified;
                            cellFailed.errorType{jCounter}        = obj.databaseRoot{failedExamIDs.examIDs(jExamID)}{jReco,4}.error{end}.type    ;
                            
                        end
                    end
                end
            end
            disp('===============================================================')
            disp('Failed jobs: ')
            if jCounter
                disp(table(cell2mat(cellFailed.exID)',cellFailed.invoked',cellFailed.recoFct',cellFailed.ACQ_scan_name',cellFailed.errorDate',cellFailed.errorType','VariableNames',{'examID','invoked','recoFct','scanName','errorDate','errorType'}))
            else
                disp('(NONE)');
            end
                    
        end
        
        
        
        
        function cleanup(obj)
            % deleting error information and old scripts
            failedRecos = obj.show('failed');
            if ~isempty(failedRecos)
                examCounter = 0;
                for examID = failedRecos.examIDs
                    examCounter = examCounter +1;
                    for jReco = numel(failedRecos.details{examCounter}):-1:1
                        if failedRecos.details{examCounter}(jReco)
                            obj.databaseRoot{examID}(jReco,:) = [];
                        end
                    end
                end
            end
            newRecos = obj.show('new');
            if ~isempty(newRecos)
                examCounter = 0;
                for examID = newRecos.examIDs
                    examCounter = examCounter +1;
                    for jReco = numel(newRecos.details{examCounter}):-1:1
                        if newRecos.details{examCounter}(jReco)
                            obj.databaseRoot{examID}(jReco,:) = [];

                        end
                    end
                end
            end
        end
        
        
       function value = recoError(obj,examID,recoID,errorType)
            if nargin < 4 %get error information
                value = obj.databaseRoot{examID}{recoID,4}.error;
            else % set error
                nError = size(obj.databaseRoot{examID}{recoID,4}.error,2)+1;
                obj.databaseRoot{examID}{recoID,4}.error{nError}.type = errorType;
                obj.databaseRoot{examID}{recoID,4}.error{nError}.identified = datestr(now);
            end
        end
        
            
        
    end
    
    methods (Access = private)
        
        
        
        function value = show(obj, typeStr)
            if nargin < 2
                typeStr = '';
            end
            switch typeStr
                case 'all'
                    % find all recos that were not successful yet
                    if ~isempty(obj.databaseRoot)
                         isReco = cell2mat(cellfun( @(sas) ~isempty(sas), obj.databaseRoot, 'uni', false ));
                         isRecoIDs = 1:size(obj.databaseRoot,2);
                         value = isRecoIDs(isReco);
                    else
                        value = [];
                    end
                case 'incomplete'
            
                    isRecoIDs = obj.show('all');
                    
                    if ~isempty(isRecoIDs)
                        nonSuccessfulRecos = cellfun( ...
                            @(sas) cell2mat(cellfun( @(SAS) (isempty(SAS.complete) || SAS.complete == 0), sas(:,4), 'uni', false) ...
                            ), obj.databaseRoot(isRecoIDs), 'uni', false );

                        numberOfNonSuccessfulScans = cell2mat(cellfun( @(sas) nnz(sas), nonSuccessfulRecos, 'uni', false));

                        value.examIDs = isRecoIDs(numberOfNonSuccessfulScans > 0);
                        value.details = nonSuccessfulRecos(numberOfNonSuccessfulScans  > 0);
                        value.numberOfIncompleteRecos = numberOfNonSuccessfulScans(numberOfNonSuccessfulScans > 0);
                    else
                        value = [];
                    end
                case 'running'
            
                    isRecoIDs = obj.show('all');
                    if ~isempty(isRecoIDs)
                        runningRecos = cellfun( ...
                            @(sas) cell2mat(cellfun( @(SAS) (SAS.running ~= 0), sas(:,4), 'uni', false) ...
                            ), obj.databaseRoot(isRecoIDs), 'uni', false );

                        numberOfRunningScans = cell2mat(cellfun( @(sas) nnz(sas), runningRecos, 'uni', false));

                        value.examIDs = isRecoIDs(numberOfRunningScans > 0);
                        value.details = runningRecos(numberOfRunningScans  > 0);
                        value.numberOfRunningRecos = numberOfRunningScans(numberOfRunningScans > 0);
                    else
                        value = [];
                    end

                    
                case 'complete'
            
                    isRecoIDs = obj.show('all');
                    if ~isempty(isRecoIDs)
                        nonSuccessfulRecos = cellfun( ...
                            @(sas) cell2mat(cellfun( @(SAS) (SAS.complete == true), sas(:,4), 'uni', false) ...
                            ), obj.databaseRoot(isRecoIDs), 'uni', false );

                        numberOfSuccessfulScans = cell2mat(cellfun( @(sas) nnz(sas), nonSuccessfulRecos, 'uni', false));

                        value.examIDs = isRecoIDs(numberOfSuccessfulScans > 0);
                        value.details = nonSuccessfulRecos(numberOfSuccessfulScans  > 0);
                        value.numberOfCompleteRecos = numberOfSuccessfulScans(numberOfSuccessfulScans > 0);
                    else
                        value = [];
                    end
                case 'new'
                    incompleteRecos = obj.show('incomplete');
                    
                    if ~isempty(incompleteRecos)
                        newRecos = cellfun( ...
                            @(sas) cell2mat(cellfun( @(SAS) (isempty(SAS.invoked)), sas(:,4), 'uni', false) ...
                            ), obj.databaseRoot(incompleteRecos.examIDs), 'uni', false );

                        numberOfNewRecos = cell2mat(cellfun( @(sas) nnz(sas), newRecos, 'uni', false));

                        value.examIDs = incompleteRecos.examIDs(numberOfNewRecos > 0);
                        value.details = newRecos(numberOfNewRecos  > 0);
                        value.numberOfNewRecos = numberOfNewRecos(numberOfNewRecos > 0);
                    else
                        value = [];
                    end
                    
                case 'waiting'
                    incompleteRecos = obj.show('incomplete');
                    
                    if ~isempty(incompleteRecos)
                        waitingRecos = cellfun( ...
                            @(sas) cell2mat(cellfun( @(SAS) (~isempty(SAS.invoked) && ~isempty(SAS.waitForResultFile)), sas(:,4), 'uni', false) ...
                            ), obj.databaseRoot(incompleteRecos.examIDs), 'uni', false );

                        numberOfWaitingRecos = cell2mat(cellfun( @(sas) nnz(sas), waitingRecos, 'uni', false));

                        value.examIDs = incompleteRecos.examIDs(numberOfWaitingRecos > 0);
                        value.details = waitingRecos(numberOfWaitingRecos  > 0);
                        value.numberOfWaitingRecos = numberOfWaitingRecos(numberOfWaitingRecos > 0);
                    else
                        value = [];
                    end    
                    
                case 'failed'
                    checkResult = obj.checkReco;
                    if ~isempty(checkResult)
                        value = checkResult.failedRecos;
                    else
                        value = [];
                    end
                otherwise
                    dispstatus('Use all, new, complete, incomplete, running, or failed.')
            end
        end
        
        function value = genResultFolder(obj,examID,brukerDbInstance)
            
            examName = brukerDbInstance.show(examID).examName;
            examName = examName ~= '/' & examName ~= 0 & examName ~= ' ';
            examName = brukerDbInstance.show(examID).examName(examName);
            
            animalID = brukerDbInstance.show(examID).animalID;
            animalID = animalID ~= '/' & animalID ~= 0 & animalID ~= ' ';
            animalID = brukerDbInstance.show(examID).animalID(animalID);
            
            value = fullfile(obj.resultsBaseFolder,['SU' animalID],['EX' num2str(examID,'%06.0f') '_' examName]);
        end
        
        
        
        function value = checkReco(obj,setAllToUnsuccessful,isCheckNew,recoDef,recoDefNos,isCheckComplete)
            % checks if 'incomplete' reconstructions were successful, i.e. all files
            % exist and shell script has been deleted
            
            if nargin < 2 || isempty(setAllToUnsuccessful)
                setAllToUnsuccessful = false;
            end
            
            if nargin < 3 || (~isempty(isCheckNew) && ~isCheckNew)
                incompleteRecos = obj.show('incomplete');
            elseif nargin < 6 || ~isempty(isCheckComplete)
                incompleteRecos = obj.show('complete');
            else
                incompleteRecos = obj.show('new');
            end
            
            if ~isempty(incompleteRecos)
                examCounter = 0;
                value.failedRecos = incompleteRecos;
                if nargin < 3 || (~isempty(isCheckNew) && ~isCheckNew)
                    value.failedRecos = rmfield(value.failedRecos,'numberOfIncompleteRecos');
                elseif nargin < 6 || ~isempty(isCheckComplete)
                    value.failedRecos = rmfield(value.failedRecos,'numberOfCompleteRecos');
                else
                    value.failedRecos = rmfield(value.failedRecos,'numberOfNewRecos');
                end
                %revalue.failedRecos.details = cellfun( @(sas) ~sas, value.failedRecos.details, 'uni', false);

                if ~setAllToUnsuccessful
                    exIDs = incompleteRecos.examIDs;
                else
                    exIDs = obj.show('all');
                end
                for jExam = exIDs
                    examCounter = examCounter + 1;

                    if ~setAllToUnsuccessful
                        recoIdNotSuccessful = 1:numel(incompleteRecos.details{examCounter});
                    else
                        if nargin > 3 && ~isempty(recoDefNos)
                            recoIdNotSuccessful = [];
                            for jReco = 1:size(obj.databaseRoot{jExam},1)
                                for jRecoDefNo = recoDefNos
                                    if strcmp(obj.databaseRoot{jExam}{jReco,1},cleanString(recoDef{jRecoDefNo}.ACQ_scan_name)) && ...
                                        strcmp(obj.databaseRoot{jExam}{jReco,3},recoDef{jRecoDefNo}.recoFct)
                                        recoIdNotSuccessful = [recoIdNotSuccessful,jReco];
                                    end
                                end
                            end
                        else
                            recoIdNotSuccessful = 1:size(obj.databaseRoot{jExam},1);
                        end
                    end




                    if ~setAllToUnsuccessful
                         recoIdNotSuccessful = recoIdNotSuccessful(incompleteRecos.details{examCounter});
                    end

                    for jReco = recoIdNotSuccessful
                        %if ~isempty(obj.databaseRoot{jExam}{jReco,1})
                            %if obj.databaseRoot{jExam}{jReco,4}.running % supposed to be running
                            value.failedRecos.details{examCounter}(jReco) = false;
                            if setAllToUnsuccessful
                                obj.databaseRoot{jExam}{jReco,4}.complete = false;
                                obj.databaseRoot{jExam}{jReco,4}.running = false;
                                obj.databaseRoot{jExam}{jReco,4}.invoked = [];
                            elseif ~obj.existScript(incompleteRecos.examIDs(examCounter),jReco)
                                
                                if obj.existReco(jExam,jReco)
                                    obj.databaseRoot{jExam}{jReco,4}.complete = true;
                                elseif isfield(obj.databaseRoot{jExam}{jReco,4},'invoked') &&  ~isempty(obj.databaseRoot{jExam}{jReco,4}.invoked)
                                    if isempty(obj.databaseRoot{jExam}{jReco,4}.waitForResultFile) % this reco is NOT just waiting for creation of file in resultsfolder
                                        % true error:
                                        % script does not exist, i.e. reco
                                        % finished, but no results
                                        value.failedRecos.details{examCounter}(jReco) = true;
                                        
                                        if obj.databaseRoot{jExam}{jReco,4}.running || (nargin >= 6 && ~isempty(isCheckComplete) && isCheckComplete)
                                            obj.recoError(incompleteRecos.examIDs(examCounter),jReco,'Shell script has been deleted, but results are incomplete (compare with definitions in caller function). Most likely reconstruction crashed/failed.');
                                            obj.databaseRoot{jExam}{jReco,4}.complete = false;
                                        end
                                    end
                                end
                                obj.databaseRoot{jExam}{jReco,4}.running = false;
                                
                                
                            elseif (obj.existScript(incompleteRecos.examIDs(examCounter),jReco) && obj.isExpired(incompleteRecos.examIDs(examCounter),jReco)) % reco takes too long or crashed
                                value.failedRecos.details{examCounter}(jReco) = true;
                                
                                if obj.databaseRoot{jExam}{jReco,4}.running
                                    obj.recoError(incompleteRecos.examIDs(examCounter),jReco,'Script exists, but expired. Likely a cron job failure or reconstruction freezed.');
                                    obj.databaseRoot{jExam}{jReco,4}.running = false;
                                end
                                
                            end
                            
                            %end
                            
                        %end
                    end
                end

                numberOfFailedRecos = cell2mat(cellfun( @(sas) nnz(sas), value.failedRecos.details, 'uni', false));

                value.failedRecos.examIDs = value.failedRecos.examIDs(numberOfFailedRecos > 0);
                value.failedRecos.details = value.failedRecos.details(numberOfFailedRecos  > 0);
                value.failedRecos.numberOfFailedRecos = numberOfFailedRecos(numberOfFailedRecos > 0);
            else
                value = [];
            end
            
            function strOut = cleanString(inputStr)
                % some characters cannot be used for fields.
                strOut = strrep(inputStr,'-','_');
            end
        end
        
        
        function isExistFiles = existReco(obj,examID,recoID)
            isExistFiles = true;
            if ~isempty(obj.databaseRoot{examID}{recoID,4}) && isfield(obj.databaseRoot{examID}{recoID,4},'resultFiles') && ~isempty(obj.databaseRoot{examID}{recoID,4}.resultFiles)
                for jFile = 1:numel(obj.databaseRoot{examID}{recoID,4}.resultFiles)
                    if ~exist(obj.databaseRoot{examID}{recoID,4}.resultFiles{jFile},'file')
                        isExistFiles = false;
                    end
                end
            else
                %scripts not even created 
                isExistFiles = false;
            end
                    
        end
        
        function obj = load(obj,filename)
            if isempty(filename)
                error('Must supply a database location.')
            elseif ischar(filename) && exist(filename,'file')
                loadedDb = load(filename);
                obj.databaseRoot = loadedDb.dbVar;
                obj.brukerDB_id  = loadedDb.dbID;
                if obj.dbVersion ~= loadedDb.DB_VERSION
                    error('DB version incompatible.')
                end
                obj.status = 'original';
            else
                obj.databaseRoot = [];
                obj.brukerDB_id = [];
                obj.status = 'new';
            end
            obj.dbLocation = filename;
        end
        
        function value = findNewExams(obj,studyDBObj)
            % check for new entries in study DB
            latestExam_studyDB = studyDBObj.show('last');
            if ~isempty(obj.databaseRoot)
                existingCases = cell2mat(cellfun( @(sas) ~isempty(sas), obj.databaseRoot, 'uni', false));
                value = 1:numel(existingCases);
                value = value(~existingCases);
                value = [value,numel(existingCases)+1:latestExam_studyDB.examID];
            else
                value = 1:latestExam_studyDB.examID;
            end
        end
        
        function value = isInvoked(obj,examID,recoID)
            value = ~isempty(obj.databaseRoot{examID}{recoID,4}.invoked);
        end
        
        function value = isRunning(obj,examID,recoID)
            value = obj.databaseRoot{examID}{recoID,4}.running;
        end
        
        function value = existScript(obj,examID,recoID)
            if ~isfield(obj.databaseRoot{examID}{recoID,4},'script')
                value = false;
            else
                value = exist(obj.databaseRoot{examID}{recoID,4}.script,'file');
            end
        end
            
        function value = isExpired(obj,examID,recoID)
            if obj.isInvoked(examID,recoID)
                if ~isempty(obj.databaseRoot{examID}{recoID,4}.maxRunTime)
                    value = datenum(obj.databaseRoot{examID}{recoID,4}.invoked)-datenum(date) > obj.databaseRoot{examID}{recoID,4}.maxRunTime;
                else
                    value = false;
                end
            else
                value = [];
            end
        end
        
        
            
        function sendErrorEmails(obj,recipient,nErrorThreshold)
            failedRecos = obj.show('failed');
            initializesendmail('ctrc-mri@bnac.net','bnac')
            
            for jExam = failedRecos.examIDs
                emailSubject = ['[reco ERROR] exam ID ' num2str(jExam)];
                emailContent = ['Reconstruction failed more than ' num2str(nErrorThreshold) ' times in the following cases: ' 10 10 10 10];
                nError = 0;
                for jReco = 1:numel(failedRecos.details,2)
                    if numel(obj.recoError(jExam,jReco)) > nErrorThreshold
                        emailContent = [emailContent,  obj.databaseRoot{jExam}{jReco,2} 10];
                        emailContent = [emailContent,  obj.databaseRoot{jExam}{jReco,3} 10];
                        errorInfo = obj.recoError(jExam,jReco);
                        emailContent = [emailContent,  'Message: ' errorInfo{end}.type 10];
                        emailContent = [emailContent,  'Occured: ' errorInfo{end}.identified 10];
                        obj.databaseRoot{jExam}{jReco,4}.notifiedFailed = now;
                        nError = nError + 1;
                    end
                end
                if nError
                    sendmail(recipient,emailSubject,emailContent)
                    dispstatus('Email sent.')
                end
            end
        end
                        
                
            
        
        
        function obj = add(obj,studyDbInstance,examIDs,definitionCell) % add exams to DB
            % studyDbInstance   - study database with studies
            % examIDs           - list of exam IDs to be added
            % definitionCell    - cell with definition of reconstruction to
            %                     be performed
             
%             if nargin < 5 || isempty(isUpdate)
%                 isUpdate = false;
%             end
            
             for jExam = examIDs
                currentExam = studyDbInstance.show(jExam);
                
                recoCount = 0;
                for jScanTypes = 2:size(definitionCell,1)
                    scanTypeString = ['scan__' cleanString(definitionCell{jScanTypes}.ACQ_scan_name)];
                    if isfield(definitionCell{jScanTypes},'afterDescr')
                        scanTypeString = [scanTypeString '___' definitionCell{jScanTypes}.afterDescr];
                    end
                    if isfield(currentExam,scanTypeString)
                        scansFolders = currentExam.(scanTypeString);
                        for jScanFolders = 1:size(scansFolders,2)
                            recoCount = recoCount + 1;
                            if jExam > size(obj.databaseRoot,2) || recoCount > size(obj.databaseRoot{jExam},1) || ~(strcmp(obj.databaseRoot{jExam}{recoCount,1},cleanString(definitionCell{jScanTypes}.ACQ_scan_name)) && strcmp(obj.databaseRoot{jExam}{recoCount,3},definitionCell{jScanTypes}.recoFct))
                                
                                obj.databaseRoot{jExam}{recoCount,1} = cleanString(definitionCell{jScanTypes}.ACQ_scan_name);
                                obj.databaseRoot{jExam}{recoCount,2} = scansFolders{jScanFolders};
                                obj.databaseRoot{jExam}{recoCount,3} = definitionCell{jScanTypes}.recoFct;   % reco function
                            

                                obj.databaseRoot{jExam}{recoCount,4}.invoked = [];            % date invoked
                                obj.databaseRoot{jExam}{recoCount,4}.complete = [];           % date found successful
                                obj.databaseRoot{jExam}{recoCount,4}.error = [];              % information about error
                                obj.databaseRoot{jExam}{recoCount,4}.notifiedFailed = [];     % date operator notified about failed reco
                                obj.databaseRoot{jExam}{recoCount,4}.resultFiles = [];        % files indicating that reco was successful
                                obj.databaseRoot{jExam}{recoCount,4}.script = [];             % created reconstruction shell script
                                obj.databaseRoot{jExam}{recoCount,4}.maxRunTime = [];         % maximum run time
                                obj.databaseRoot{jExam}{recoCount,4}.running = false;         % supposed to be running
                                obj.databaseRoot{jExam}{recoCount,4}.reqRamGB = definitionCell{jScanTypes}.reqRamGB;                      % required RAM for reconstruction, in GB
                                obj.databaseRoot{jExam}{recoCount,4}.reqCPU   = definitionCell{jScanTypes}.reqCpuNo;                      % required number of CPUs for reconstruction
                                if isfield(definitionCell{jScanTypes},'waitForResultFile')
                                    obj.databaseRoot{jExam}{recoCount,4}.waitForResultFile  = definitionCell{jScanTypes}.waitForResultFile;   % reconstruction does not start if this result file does not exist
                                else
                                    obj.databaseRoot{jExam}{recoCount,4}.waitForResultFile  = [];
                                end
                            end
                        end
                    end
                end
             end
             
             
             function strOut = cleanString(inputStr)
                % some characters cannot be used for fields.
                strOut = strrep(inputStr,'-','_');
                strOut = strrep(strOut,' ','_');
                strOut = strrep(strOut,'(','_');
                strOut = strrep(strOut,')','_');
                % make sure changes here are also made in
                % studyDB and BRUKERDATACOMPLETENESSCHECK
            end
             
        end
    
end

end