
# this code is being sourced into each study's reco-run script.

source ${BASEFOLDER}/system/studies.conf

scriptFileName=$2
searchString=$3
scriptFileName="${scriptFileName}.sh"


if [ $# -ge 3 ]; then
     cd ${recoScripts}
     currDir=`pwd`

     commandStr="ls *${searchString}* -1 > $scriptFileName"
     eval $commandStr

     commandStr="sed -i '/${scriptFileName}/d' $scriptFileName"
     eval $commandStr

     commandStr="sed -i 's/SU/env -i .\/SU/g' $scriptFileName"
     eval $commandStr
     sudo chmod 777 . -R
     sudo chmod 777 ../reco_data -R
     chmod 777 $scriptFileName

     echo "Entering the password (${reco_userpwd}) will start the serial execution on the local machine."
     su ${reco_username} -c "env -i ./$scriptFileName"
     #su lcmodel -c "env -i $scriptFileName"

     echo "Execution has completed."
     rm ${scriptFileName}

else

     echo "Need to supply at least 1 arguments: scriptFilename (filterString)"

fi

