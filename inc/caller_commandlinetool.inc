
%%% This code is used for shell command caller scripts. 
%%% Usually, no need to modify this script

headerCode = fileread(fullfile(systemIncludesLocation,'caller_header.inc'));
eval(headerCode)


for jExec = 1:size(executable,2)
    if exist('environmentInit','var') && size(environmentInit,1) >= jExec
        for jEnvInit = 1:size(environmentInit,2)
            if ~isempty(environmentInit{jExec,jEnvInit})
                shellCommand = [shellCommand environmentInit{jExec,jEnvInit} ' \n'];
            end
        end
    end
    shellCommand = [shellCommand executable{jExec} ' '];
    for jArgument = 1:size(inputArguments,2)
        shellCommand = [shellCommand inputArguments{jExec,jArgument} ' '];
    end
    shellCommand = [shellCommand ' \n'];
end

footerCode = fileread(fullfile(systemIncludesLocation,'caller_footer.inc'));
eval(footerCode)
