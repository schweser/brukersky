
%%% This code creates is used for pymatlab caller scripts. 
%%% Usually, no need to modify this script


headerCode = fileread(fullfile(systemIncludesLocation,'caller_header.inc'));
eval(headerCode)


logFile = fullfile(resultFolder,[matlabFct,'.log']);
shellCommand = [shellCommand 'pymatlab -m ' matlabFct ' -p ' fullfile(hostStudyBaseFolder,matlabFctLocation) ' -l ' fullfile(hostStudyBaseFolder,logFile) ' -ma'];
for jArgument = 1:size(matlabFctArguments,2)
    shellCommand = [shellCommand ' "''' matlabFctArguments{jArgument} '''"'];
end
shellCommand = [shellCommand ' \n'];

footerCode = fileread(fullfile(systemIncludesLocation,'caller_footer.inc'));
eval(footerCode)

