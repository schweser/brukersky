
if exist('resultFiles','var')
    shellCommand = [shellCommand 'if '];
    for jResultfile = 1:size(resultFiles,2)
        if jResultfile > 1
            shellCommand = [shellCommand ' && '];
        end
        shellCommand = [shellCommand '[ -f ' fullfile(hostStudyBaseFolder,resultFolder,resultFiles{jResultfile}) ' ]'];
    end
    shellCommand = [shellCommand '; \n'];
    shellCommand = [shellCommand 'then \n'];
    shellCommand = [shellCommand '    rm ' scriptFileName '\n'];
    shellCommand = [shellCommand 'else \n'];
    shellCommand = [shellCommand '    mkdir -p ERROR_results_incomplete \n'];
    shellCommand = [shellCommand '    mv ' scriptFileName ' ERROR_results_incomplete \n'];
    shellCommand = [shellCommand 'fi \n'];
end

fid = fopen(scriptFileName,'wt');
fprintf(fid,shellCommand);
fclose(fid);

maxRunTime = 1; % in days