#!/bin/bash

if (( $# == 0 )); then
    >&2 echo "Illegal number of parameters"
fi

STUDYID=$1

source config/base.config

STUDYBASEFOLDER="${BASEFOLDER}/studies/${STUDYID}"

source study_update.inc
