# this code is being sourced into each study's study-update script.

source studies.conf

# study-instance specific settings
source ${STUDYBASEFOLDER}/config/docker.def

commandStr="cd /app; update_docker('${STUDYID}')"

if [ $# -eq 2 ]; then
  debugPort="$2"
  dockerArgs=" --rm -it --mac-address=${licenseHostID} -p ${debugPort}:5901"
  arguments="xterm"
  echo "Starting container in debug mode."
  echo "Connect to VNC on port ${debugPort} with password kjdfseYwh and start matlab. Then run this command:"
  echo "  ${commandStr}"
  commandStr=""
else
  dockerArgs=" --rm --mac-address=${licenseHostID} --entrypoint=matlab"
  arguments="-softwareopengl -r"
fi;


docker run ${dockerArgs} \
   -v $dataOriginalPath:/data_original:ro \
   -v ${licenseDir}:/usr/local/MATLAB/${matlabversion}/licenses/:ro \
   -v $globalDbPath:/db_global:ro \
   -v $recoModules:/reco_modules:ro \
   -v $recoScripts:/reco_scripts:rw \
   -v $recoData:/reco_data:rw \
   -v $confPath:/conf:ro \
   -v $dbPath:/db:rw \
   -v $globalDbConfPath:/conf_global:ro \
   -v $globalDbProgramPath:/app:ro \
   brukersky_matlab:r2018b ${arguments} "${commandStr}"
