function installInstance(instanceFolder)

if ~exist(instanceFolder,'dir')
    mkdir(instanceFolder)
end

currentDirectory    = fileparts(mfilename('fullpath'));
distributionDir     = fullfile(currentDirectory,'distribution');

if ~exist(fullfile(instanceFolder,'system'),'dir')
    mkdir(fullfile(instanceFolder,'system'))
end

commandLine = ['cp ' fullfile(distributionDir) ' ' fullfile(instanceFolder,'system') ' -R'];
system(commandLine);

commandLine = ['cp ' fullfile(currentDirectory,'source','functions','update.m') ' ' instanceFolder];
system(commandLine);

end