% This script builds a distributable version of BrukerSky
% Call this code by calling script.


currentDirectory = fileparts(mfilename('fullpath'));
distributionDir = fullfile(currentDirectory,'distribution');

commandLine = ['rm ' fullfile(distributionDir) ' -R'];
system(commandLine);

if ~exist(distributionDir,'dir')
    mkdir(distributionDir)
end
cd(distributionDir)

% for distribution purposes
% pcode(fullfile(currentDirectory,'src','functions','*.m'))
% pcode(fullfile(currentDirectory,'src','class','*.m'))
% pcode(fullfile(currentDirectory,'src','external_fromBruker','*.m')) % will have to be replaced because 3rd party
% delete(fullfile(distributionDir,'update.p'));


ls
cd(currentDirectory)


commandLine = ['cp ' fullfile(currentDirectory,'inc') ' ' fullfile(distributionDir) ' -R'];
system(commandLine);

% for internal purposes
commandLine = ['cp ' fullfile(currentDirectory,'src/functions/*') ' ' fullfile(distributionDir) ' -R'];
system(commandLine);
commandLine = ['cp ' fullfile(currentDirectory,'src/class/*') ' ' fullfile(distributionDir) ' -R'];
system(commandLine);
commandLine = ['cp ' fullfile(currentDirectory,'src/external_fromBruker/*') ' ' fullfile(distributionDir) ' -R'];
system(commandLine);
commandLine = ['cp ' fullfile(currentDirectory,'src/external_other/*') ' ' fullfile(distributionDir) ' -R'];
system(commandLine);


