
%%% This code creates docker matlab caller scripts. 
%%% Usually, no need to modify this script

global hostStudyBaseFolder
global hostDbBaseFolder

licenseHostID		= 'E4:CE:8F:60:8F:0A';
softwarePath		= '/shared/software/';
tmpPath			= '/synology/projects/computation_temporary_files/';
licensePath             = fullfile(hostDbBaseFolder,'/licenses');
dcm2niiPath		= '/usr/bin/';
dockerImage		= 'brukersky_matlab:r2018b';

headerCode = fileread(fullfile(systemIncludesLocation,'caller_header_nomkdir.inc'));
eval(headerCode)

logFile = fullfile(hostStudyBaseFolder,resultFolder,[matlabFct,'.log']);



matlabCmd = ['cd ' matlabFctLocation '; '];
matlabCmd = [matlabCmd matlabFct '('];
for jArgument = 1:size(matlabFctArguments,2)
    if jArgument > 1
        matlabCmd = [matlabCmd ','];
    end
    matlabCmd = [matlabCmd '''' matlabFctArguments{jArgument} ''''];
end
matlabCmd = [matlabCmd ')'];


shellCommand = [shellCommand 'mkdir ' fullfile(hostStudyBaseFolder,resultFolder) ' -p\n'];


dockerCommand = 'docker run ${dockerArgs} \\\n';
dockerCommand = [dockerCommand '   -v ' fullfile(hostDbBaseFolder,scanFolder) ':' scanFolder ':ro \\\n'];
dockerCommand = [dockerCommand '   -v ' fullfile(hostStudyBaseFolder,resultFolder) ':' resultFolder ':rw \\\n'];

dockerCommand = [dockerCommand '   -v ' fullfile(softwarePath,'vendor','atlases') ':' fullfile(softwarePath,'vendor','atlases') ':ro \\\n'];
dockerCommand = [dockerCommand '   -v ' fullfile(softwarePath,'matlab','nifti') ':' fullfile(softwarePath,'matlab','nifti') ':ro \\\n'];

%dockerCommand = [dockerCommand '   -v ' tmpPath ':/tmp:rw \\\n'];
dockerCommand = [dockerCommand '   -v ' licensePath '/matlab/R2018a:/usr/local/MATLAB/R2018a/licenses/:ro \\\n'];
dockerCommand = [dockerCommand '   -v ' licensePath '/freesurfer/license.txt:/opt/freesurfer-6.0.0/license.txt:ro \\\n'];
%dockerCommand = [dockerCommand '   -v ' dcm2niiPath ':/usr/bin/:ro \\\n'];

dockerCommand = [dockerCommand '   -v ' fullfile(hostStudyBaseFolder,matlabFctLocation) ':' matlabFctLocation ':ro \\\n'];
dockerCommand = [dockerCommand '   ' dockerImage];




% debug option
shellCommand = [shellCommand 'if [ $# -eq 1 ]; then\n'];
shellCommand = [shellCommand '  debugPort="$1"\n'];
shellCommand = [shellCommand '  dockerArgs=" --rm -it --mac-address=' licenseHostID ' -p ${debugPort}:5901"\n'];
shellCommand = [shellCommand '  arguments="xterm"\n'];
shellCommand = [shellCommand '  echo "Starting container in debug mode."\n'];
shellCommand = [shellCommand '  echo "Connect to VNC on port ${debugPort} with password kjdfseYwh and start matlab. Then run this command:"\n'];
shellCommand = [shellCommand '  echo "  ' matlabCmd '"\n'];
shellCommand = [shellCommand '  ' dockerCommand ' ${arguments}\n'];
shellCommand = [shellCommand 'else\n'];
shellCommand = [shellCommand '  logFileArgument="' logFile '"\n'];
shellCommand = [shellCommand '  dockerArgs=" --rm --mac-address=' licenseHostID ' --entrypoint=matlab"\n'];
shellCommand = [shellCommand '  arguments="-softwareopengl -r"\n'];
shellCommand = [shellCommand '  commandStr="' matlabCmd '"\n'];
shellCommand = [shellCommand '  ' dockerCommand ' ${arguments} "${commandStr}"'];
shellCommand = [shellCommand ' > ${logFileArgument}\n'];
shellCommand = [shellCommand 'fi;\n\n\n'];


footerCode = fileread(fullfile(systemIncludesLocation,'caller_footer_docker.inc'));
eval(footerCode)

