
shellCommand = '#!/bin/bash \n';
shellCommand = [shellCommand '# REQRAM: ' num2str(requirements.ram) '\n'];
shellCommand = [shellCommand '# REQCPU: ' num2str(requirements.cpu) '\n'];

shellCommand = [shellCommand 'mkdir -p ' fullfile(hostStudyBaseFolder,resultFolder) '\n'];
if exist('tmpDir','var')
    for jDir = 1:size(tmpDir,2)
        shellCommand = [shellCommand 'mkdir -p ' tmpDir{jDir} '\n'];
    end
end
