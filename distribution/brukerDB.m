classdef brukerDB < handle
    
    % database class for storing meta data for bruker data on local file system.
    % The class is programmed in a way that all data is maintained and that
    % IDs are guaranteed to be unique. Several consistency checks were
    % implemented.
    %
    % The following public methods are available:
    %
    %   (constructor)       - loads/creates a (new) database file (mat-file to
    %                         be saved) based in the specified file name.
    %   .checkDataExistence - checks if all data in the database still
    %                         exists at specified locations.
    %   .save               - saves changes made to database to the
    %                         database file
    %   .add                - adds a new dataset to the database. Specify
    %                         the folder.
    %   .scan               - scans the specified folder for existence of
    %                         data that is not yet in database. Specify
    %                         folder and recursion search depth.
    %   .remove             - removes a dataset from the database. Entry
    %                         is actually only deactivated. Specify exam
    %                         ID.
    %   .recover            - Recovers removed dataset.
    %   .export             - Exports database to file. Specify either text
    %                         file name (.txt) or Excel. 
    %   .show               - shows the content of the database in table
    %                         format.
    %   .find               - searches for a dataset based on specified
    %                         search string and field. First argument is
    %                         the search string, second argument is the
    %                         type of information you want to search for.
    %                         Use .show to see available types.
    %
    % The following public properties exist:
    %
    %   dbLocation      - location of database file
    %   lastSaved       - when database was saved last
    %   status          - new/modified/saved
    
 % F Schweser - 30/04/2015 
 %
 % Changelog
 %
 %  30/04/2015 - v1.0 - F Schweser - initial
 %  16/11/2015 - v1.1 - F Schweser - Major fox of hash. Not unique.
 %  11/04/2016 - v1.2 - F Schweser - Bugfix regarding v1.1
 
    properties (Hidden = true, GetAccess = private, SetAccess = private)
        databaseRoot
        dbIdentifier
    end
    
    properties (Hidden = false, GetAccess = public, SetAccess = private)
        dbLocation
        status
        lastSaved
    end
    
    properties (Constant)
        dbVersion = 'v1.0';
    end
    
    methods
        
        %% get methods
        function obj = brukerDB(dbLocation)
            % class constructor
            
            obj.load(dbLocation);
        end
        
        
        function isExist = checkDataExistence(obj,dataStorageFolder)
            isExist = true;
            for jElement = 1:size(obj.databaseRoot,1)
                if ~obj.databaseRoot{jElement,1}.exist(dataStorageFolder)
                    isExist = false;
                    break
                end
            end
        end
        
        
        function value = id(obj)
            value = obj.dbIdentifier; 
        end
        
        
        function obj = save(obj)
            dbVar = obj.databaseRoot; %#ok<NASGU>
            dbID = obj.dbIdentifier; %#ok<NASGU>
            DB_VERSION = obj.dbVersion; %#ok<NASGU>
            if checkDBConsistency(obj)
                obj.lastSaved = datestr(clock,0);
                save(obj.dbLocation,'dbVar','DB_VERSION','dbID');
            else
                error('Cannot save.')
            end
            obj.status = 'saved';
        end
        
        
        
        function [obj,code] = add(obj, input, dataFolder)
            if ischar(input)
                input = brukerData(input,dataFolder);
            elseif ~isa(input,'brukerData')
                error('Input must be path to subject file or brukerData object.')
            end
            if input.check(dataFolder)
                dataHash = string2hash([input.animalID,input.studyName,input.examName,]); % we are using the old hash here for backward compatibility
                dataHashNew = 1000*string2hash(strrep(input.instanceUID,'.','')); % we are using the old hash here for backward compatibility (x1000 to distinguish from old hash)
                isEqualHdl = @(x)isEqual(x,dataHash);
                isEqualHdlNew = @(x)isEqual(x,dataHashNew);
                newID = obj.newExamID;

                % does already exist?
                if newID > 1
                    dataExistsPos        = cellfun(isEqualHdl, obj.databaseRoot(:,2));
                    dataExistsPosNewHash = cellfun(isEqualHdlNew, obj.databaseRoot(:,2));
                    
                    % since the old hash is not unique, we check the UID too if
                    % exam exists with same hash
                    
                    if nnz(dataExistsPos) && ~nnz(dataExistsPosNewHash) 
                        % old hash exists, but new one does not 
                        % now compare UID, if is same dataset
                        
                        datasetWithSameOldHash = obj.databaseRoot(find(dataExistsPos),1); %#ok<FNDSB>
                        if myisfield(datasetWithSameOldHash{1},'instanceUID') && strcmp(datasetWithSameOldHash{1}.instanceUID,input.instanceUID)
                            % found same old hash, but UIDs are identical
                            dataExistsPosNewHash = dataExistsPos;
                        else
                            % if we don't have the field instanceUID, or we
                            % have it and IUDs are not identical we change
                            % variables dataHash and dataExistsPos to the
                            % new-hash analogues.
                            dataHash = dataHashNew;
                            dataExistsPos = dataExistsPosNewHash; % sets dataExistsPos to all zeros
                        end
                    end
                end

                
                if newID == 1 || (~nnz(dataExistsPos) || ~nnz(dataExistsPosNewHash))
                    % Did not find matching hashes...
                    % does study already exist?
                    studyExisting = obj.find(input.studyName,'studyName');




                    if ~isempty(studyExisting)
                        obj.databaseRoot{newID,5} = obj.databaseRoot{studyExisting(1),5};
                    elseif newID == 1
                        obj.databaseRoot{newID,5} = 1;
                    else
                        obj.databaseRoot{newID,5} = max(cell2mat(obj.databaseRoot(:,5))) + 1;
                    end

                    obj.databaseRoot{newID,1} = input;
                    obj.databaseRoot{newID,2} = dataHash;
                    obj.databaseRoot{newID,3} = date;
                    obj.databaseRoot{newID,4} = true; % active

                    obj.status = 'modified';
                    code = 1;
                elseif ~obj.databaseRoot{find(dataExistsPos),4} %#ok<FNDSB>
                    obj.recover(find(dataExistsPos)); %#ok<FNDSB> % already exists as deleted entry. We just recover it.
                    code = 2;
                else
                    dispstatus(['This entity already exists as examID ' num2str(find(dataExistsPos))]);
                    code = 3;
                end
            else
                dispstatus(['Data not added because of problem with study file: ' input.location]);
                code = 4;
            end
        end
        
        function coundAdded = scan(obj, folder,recursionDepth,noSudo)
            if nargin < 3 || isempty(recursionDepth)
                recursionDepth = 3;
            end
            if nargin < 4 || isempty(noSudo)
                noSudo = false;
            end
            folders = dir2(folder,'subject',recursionDepth,'/s'); % can be refined wrt date
            coundAdded = 0;
            
            if ~isempty(folders)
                folders = cellfun( @(sas) fileparts(sas), folders, 'uni', false );
                
                for jFolder = 1:size(folders,2)
                    if exist(fullfile(folders{1,jFolder},'subject'),'file')
                        examID = obj.find(fullfile(folders{1,jFolder}(numel(folder)+1:end)),'location');
                        if isempty(examID)
                            newFinalDataStorageSubFolder = ['locked_' datestr(now,'yyyymmdd')];
                            if ~exist(fullfile(folder,newFinalDataStorageSubFolder),'dir')
                                mkdir(fullfile(folder,newFinalDataStorageSubFolder))
                            end
                            OptsShell.noShellCommandPrint = true;
                            [~,currentUser] = exec_system_command('whoami',OptsShell);
                            [~,ownerOfData] = exec_system_command('ls -l | awk ''{print $3}''',OptsShell);
                            if ~strcmp(currentUser,ownerOfData) && ~noSudo % We assume that you are either owning the data or you are in the sudoers list
                                sudoStr = 'sudo ';
                            else
                                sudoStr = '';
                            end

                            oldLocation = folders{1,jFolder};
                            newLocation = fullfile(folder,newFinalDataStorageSubFolder,folders{1,jFolder}(numel(fileparts(folders{1,jFolder}))+1:end));

                            newLocationStripped = fullfile(newFinalDataStorageSubFolder,folders{1,jFolder}(numel(fileparts(folders{1,jFolder}))+1:end));
                            if ~strcmp(oldLocation,newLocation) && ~exist(newLocation,'dir')
                                shellStrMv = [sudoStr 'mv ' oldLocation ' ' newLocation ' -f; ']; % move to final destination
                            else
                                shellStrMv = '';
                            end

                            shellStr = [shellStrMv sudoStr 'chmod a-w ' newLocation]; % set to read only access
                            try
                                exec_system_command(shellStr);
                            catch
                                error('Something went wrong. You must be either the owner of the data or in the sudoers list. ')
                            end
                            [~,code] = obj.add(newLocationStripped,folder);

                            if code == 4
                                if ~strcmp(shellStrMv,'')
                                    if ~exist(fullfile(folder,'error_studyFileProblem'),'dir')
                                        mkdir(fullfile(folder,'error_studyFileProblem'));
                                    end
                                    shellStrMv = [sudoStr 'mv ' newLocation ' ' fullfile(folder,'error_studyFileProblem',folders{1,jFolder}(numel(fileparts(folders{1,jFolder}))+1:end)) ' -f; '];
                                    try
                                        exec_system_command(shellStrMv);
                                    catch
                                        error(['Something went wrong. You must be either the owner of the data or in the sudoers list. '])
                                    end
                                end
                            end
                            if code == 3
                                if ~strcmp(shellStrMv,'')
                                    if ~exist(fullfile(folder,'error_studyAlreadyInDB'),'dir')
                                        mkdir(fullfile(folder,'error_studyAlreadyInDB'));
                                    end
                                    shellStrMv = [sudoStr 'mv ' newLocation ' ' fullfile(folder,'error_studyAlreadyInDB',folders{1,jFolder}(numel(fileparts(folders{1,jFolder}))+1:end)) ' -f; '];
                                    try
                                        exec_system_command(shellStrMv);
                                    catch
                                        error(['Something went wrong. You must be either the owner of the data or in the sudoers list. '])
                                    end
                                end
                            end
                            coundAdded = coundAdded + 1;
                        end
                    end
                end
            end
            dispstatus([num2str(coundAdded) ' datasets scanned.'])
        end
        
        function remove(obj,examID)
            if examID < obj.newExamID
                if obj.databaseRoot{examID,4}
                    obj.databaseRoot{examID,4} = false;
                else
                    disp('This entry has already been deleted.')
                end
            else
                error('This exam ID does not exist.');
            end
        end
        
        function recover(obj,examID)
            if examID < obj.newExamID
                if ~obj.databaseRoot{examID,4}
                    obj.databaseRoot{examID,4} = true;
                else
                    disp('This entry has not been deleted.')
                end
            else
                error('This exam ID does not exist.');
            end
        end
        
        function value = show(obj, examID,typeStr)
            if nargin < 2
                nonDeletedCases = cell2mat(obj.databaseRoot(:,4));
                animalIDs = cellfun( @(sas) sas.animalID, obj.databaseRoot(nonDeletedCases,1), 'uni', false );
                examIDs   = 1:numel(obj.databaseRoot(:,1));
                examIDs = examIDs(nonDeletedCases);
                studyNames = cellfun( @(sas) sas.studyName, obj.databaseRoot(nonDeletedCases,1), 'uni', false );
                examNames = cellfun( @(sas) sas.examName, obj.databaseRoot(nonDeletedCases,1), 'uni', false );
                locations = cellfun( @(sas) sas.location, obj.databaseRoot(nonDeletedCases,1), 'uni', false );
                Table = table(studyNames,cell2mat(obj.databaseRoot(nonDeletedCases,5)),animalIDs,examNames,examIDs',locations,obj.databaseRoot(nonDeletedCases,3),'VariableNames',{'studyName','studyID','animalID','examName','examID','location','addedToDatabase'});
                value = sortrows(Table,{'studyName','animalID','examName'});
            else
                if nargin < 3 || isempty(typeStr)
                    typeStr = 'object';
                end
                activeCases = cell2mat({obj.databaseRoot{examID,4}});
                switch typeStr
                    case 'object'
                        if nnz(activeCases) > 1
                            value = {obj.databaseRoot{examID(activeCases),1}};
                        elseif ~isempty(examID(activeCases))
                            value = obj.databaseRoot{examID(activeCases),1};
                        else
                            value = [];
                        end
                    case 'addDate'
                        if nnz(activeCases) > 1
                            value = {obj.databaseRoot{examID(activeCases),3}};
                        else
                            value = obj.databaseRoot{examID(activeCases),3};
                        end
                    otherwise
                        error('Type not supported.')
                end
            end
        end
        
        function export(obj,filename)
            writetable(obj.show,filename)
        end
        
        function value = studyID(obj,studyName)
            exIDs = obj.find(studyName,'studyName');
            if ~isempty(exIDs)
                value = obj.databaseRoot{exIDs(1),5};
            else
                value = [];
            end
        end
        
        function examID = find(obj, infoStr, type, isExcludeRemovedItems)
            if nargin < 4 || isempty(isExcludeRemovedItems)
                isExcludeRemovedItems = false;
            end
            
            if ~isempty(obj.databaseRoot)
                examID = find(strcmp( cellfun( @(sas) sas.(type), obj.databaseRoot(:,1), 'uni', false ), {infoStr}));
            else
                examID = [];
            end
            if strcmp(type,'examID') && ~isempty(examID) && ~obj.databaseRoot{examID,4}
                dispstatus('This dataset has been deleted.')
            end
            if isExcludeRemovedItems
                % eliminate removed items
                eliminationVector = true(numel(examID),1);
                for jId = 1:numel(examID)
                    if ~obj.databaseRoot{examID(jId),4}
                        eliminationVector(jId) = false;
                    end
                end
                examID = examID(eliminationVector);
            end
        end
        
        
        
    end
    
    methods (Access = private)
        
        function value = addDate(obj, examID)
            value = obj.databaseRoot{examID,3};
        end
        
%         function value = get.databaseRoot(obj)
%             value = obj.databaseRoot;
%         end
        
        function value = newExamID(obj)
            if isempty(obj.databaseRoot)
                value = 1;
            else
                value = size(obj.databaseRoot,1) + 1;
            end
        end
        
        function obj = load(obj,filename)
            if isempty(filename)
                error('Must supply a database location.')
            elseif ischar(filename) && exist(filename,'file')
                loadedDb = load(filename);
                obj.databaseRoot = loadedDb.dbVar;
                obj.dbIdentifier = loadedDb.dbID;
                if obj.dbVersion ~= loadedDb.DB_VERSION
                    error('DB version incompatible.')
                end
                obj.status = 'original';
            else
                obj.databaseRoot = [];
                obj.status = 'new';
                disp('You are creating a new database. This required creating a one-time random DB ID.')
                obj.dbIdentifier = input('Type an arbitrary random string and hit ENTER: ','s');
            end
            obj.dbLocation = filename;
        end
    
    
    function isConsistent = checkDBConsistency(obj)
            isConsistent = true;
            for jElement = 1:size(obj.databaseRoot,1)
                if ~isa(obj.databaseRoot{jElement,1},'brukerData') || ...
                        ~isa(obj.databaseRoot{jElement,2},'double') || ...
                        ~isa(obj.databaseRoot{jElement,3},'char') || ...
                        ~isa(obj.databaseRoot{jElement,4},'logical')
                    disp(['Database is inconsistent in examID ' num2str(jElement)])
                    isConsistent = false;
                end
            end
        end
    end
end

function bool=isEqual(A,B)
bool = (A==B);
end

function hash=string2hash(str,type)
% This function generates a hash value from a text string
%
% hash=string2hash(str,type);
%
% inputs,
%   str : The text string, or array with text strings.
% outputs,
%   hash : The hash value, integer value between 0 and 2^32-1
%   type : Type of has 'djb2' (default) or 'sdbm'
%
% From c-code on : http://www.cse.yorku.ca/~oz/hash.html
%
% djb2
%  this algorithm was first reported by dan bernstein many years ago
%  in comp.lang.c
%
% sdbm
%  this algorithm was created for sdbm (a public-domain reimplementation of
%  ndbm) database library. it was found to do well in scrambling bits,
%  causing better distribution of the keys and fewer splits. it also happens
%  to be a good general hashing function with good distribution.
%
% example,
%
%  hash=string2hash('hello world');
%  disp(hash);
%
% Function is written by D.Kroon University of Twente (June 2010)


% From string to double array
str=double(str);
if(nargin<2), type='djb2'; end
switch(type)
    case 'djb2'
        hash = 5381*ones(size(str,1),1);
        for i=1:size(str,2),
            hash = mod(hash * 33 + str(:,i), 2^32-1);
        end
    case 'sdbm'
        hash = zeros(size(str,1),1);
        for i=1:size(str,2),
            hash = mod(hash * 65599 + str(:,i), 2^32-1);
        end
    otherwise
        error('string_hash:inputs','unknown type');
end
end