classdef studyDB < handle
    
    % database class for studies. This class holds brukerData instances and
    % scans Bruker study folders for relevant scans.
    %
    % The following public methods are available:
    %
    %   (constructor)       - loads/creates a (new) database file (mat-file to
    %                         be saved) based in the specified file name.
    %   .save               - saves changes made to database to the
    %                         database file
    %   .define             - Defines relevant scans of the study. Please
    %                         look into brukerdatacompletenesscheck() for
    %                         details.
    %   .add                - adds a new Bruker exam to the study. 
    %   .remove             - removes a exam from the study. 
    %   .export             - Exports database to file. Specify either text
    %                         file name (.txt) or Excel. 
    %   .show               - shows the content of the database in table
    %                         format (when called without arguments) or
    %                         shows detailed information about given examID.
    %
    % The following public properties exist:
    %
    %   dbLocation      - location of database file
    %   lastSaved       - when database was saved last
    %   status          - new/modified/saved
    %   dbVersion       - version number of this function
    
 % F Schweser - 5/05/2015 
 %
 % Changelog
 %
 %  5/05/2015 - v1.0 - F Schweser - initial
 %  2015/09/30 - v2.1 - F Schweser - Now accepts afterDescr without after
 %  2016/04/12 - v2.2 - F Schweser - Bugfix for exam wo scans    
    properties (Hidden = true, GetAccess = private, SetAccess = private)
        databaseRoot
        definition
        brukerDB_id
    end
    
    properties (Hidden = false, GetAccess = public, SetAccess = private)
        dbLocation
        status
        lastSaved
    end
    
    properties (Constant)
        dbVersion = 'v1.0';
    end
    
    methods
        
        %% get methods
        function obj = studyDB(dbLocation)
            % class constructor
            
            obj.load(dbLocation);
        end
        
        function obj = save(obj)
            dbVar = obj.databaseRoot; %#ok<NASGU>
            dbID = obj.brukerDB_id; %#ok<NASGU>
            DB_VERSION = obj.dbVersion; %#ok<NASGU>
            dbDefinition = obj.definition; %#ok<NASGU>
            obj.lastSaved = datestr(clock,0);
            if ~exist(fileparts(obj.dbLocation),'dir')
                mkdir(fileparts(obj.dbLocation))
            end
            save(obj.dbLocation,'dbVar','DB_VERSION','dbID','dbDefinition');
            obj.status = 'saved';
        end
        
        
        
        function obj = define(obj, expectedCell) %define the study
            if ~iscell(expectedCell)
                error('Input error.')
            end
            
            if isempty(obj.definition)
                
                obj.definition = expectedCell;
                
            else
                error('This study has already been defined. Use change member.')
            end
        end
        
        function obj = change(obj, expectedCell,dataStorageFolder,brukerDB) %changes definition of the study
            if ~iscell(expectedCell)
                error('Input error.')
            end
            
            if ~isempty(obj.definition)
                
                obj.definition = expectedCell;
                obj.update(dataStorageFolder,brukerDB);
            else
                dispstatus('This study has not yet been defined.')
            end
        end
        
        
        
         function obj = add(obj, brukerDB, examIDs,dataStorageFolder,isForceUpdate) 
             
             if nargin < 5
                 isForceUpdate = false;
             end
             
             if ~isa(brukerDB,'brukerDB') || ~isnumeric(examIDs)
                 error('Input error.')
             end
             
             if isempty(obj.definition)
                 error('Must define study first. Use define-member.')
             elseif ~isempty(obj.brukerDB_id) && ~strcmp(brukerDB.id,obj.brukerDB_id)
                 error('Incompatible database.')
             end
             
             
             
             for jExID = 1:numel(examIDs)
                 if isForceUpdate || size(obj.databaseRoot,1) < examIDs(jExID) || isempty(obj.databaseRoot{examIDs(jExID),1})
                     obj.databaseRoot{examIDs(jExID),1} = brukerDB.show(examIDs(jExID));
                     [obj.databaseRoot{examIDs(jExID),2},tmpCell] = brukerdatacompletenesscheck(brukerDB.show(examIDs(jExID)).location,obj.definition,false,[],dataStorageFolder);%fullfile(db.show(jDir).location,'dataInfo.mat'));
                     obj.databaseRoot{examIDs(jExID),3} = tmpCell{3};
                     obj.status = 'modified';
                 end
             end
         end
         
        
        function remove(obj,examID)
            if examID <= size(obj.databaseRoot,1)
                for jElement = 1:size(obj.databaseRoot,2)
                    obj.databaseRoot{examID,jElement} = [];
                    obj.status = 'modified';
                end
            end
        end
        
        function value = show(obj, examID)
            if isempty(obj.databaseRoot)
                value = [];
            elseif nargin < 2
                
                 examInStudy = cell2mat(cellfun( @(sas) ~isempty(sas), obj.databaseRoot(:,1), 'uni', false ));
                 examWithoutScans = cell2mat(cellfun( @(sas) isempty(sas), obj.databaseRoot(:,2), 'uni', false ));
                 examIDs = 1:size(obj.databaseRoot,1);
                 examIDs = examIDs(examInStudy & ~examWithoutScans);
                 
                 animalIDs = cellfun( @(sas) sas.animalID, obj.databaseRoot(examIDs,1), 'uni', false );
                 examNames = cellfun( @(sas) sas.examName, obj.databaseRoot(examIDs,1), 'uni', false );
                 
                 for jRequirement = 1:size(obj.definition,2)
                    if isfield(obj.definition{jRequirement},'afterDescr')
                        afterStr = ['___' obj.definition{jRequirement}.afterDescr];
                    else
                        afterStr = '';
                    end
                    scans{jRequirement} = cell2mat(cellfun( @(sas) sas.(['scan__' cleanString(obj.definition{jRequirement}.ACQ_scan_name) afterStr]), obj.databaseRoot(examIDs,2), 'uni', false ));
                    
                    if jRequirement == 1
                        Table = table(animalIDs,examIDs',examNames,scans{1},'VariableNames',{'animalID','examID','examName',['scan_' cleanString(obj.definition{1}.ACQ_scan_name)]});
                    else
                        Table = [Table,table(scans{jRequirement},'VariableNames',{['scan_' cleanString(obj.definition{jRequirement}.ACQ_scan_name) afterStr]})];
                    end
                 end
                 
                 value = Table;
            elseif isnumeric(examID)
                value = obj.databaseRoot{examID,3};
                value.examID = examID;
            else
                switch examID
                    case 'last'
                        value = obj.databaseRoot{end,3};
                        value.examID = size(obj.databaseRoot,1);
                    otherwise
                        error('not supported')
                end
            end
            
            function strOut = cleanString(inputStr)
                % some characters cannot be used for fields.
                strOut = strrep(inputStr,'-','_');
                strOut = strrep(strOut,' ','_');
                strOut = strrep(strOut,'(','_');
                strOut = strrep(strOut,')','_');
                % make sure changes here are also made in
                % BRUKERDATACOMPLETENESSCHECK and recoDB
            end
            
        end
        
        function export(obj,filename)
            writetable(obj.show,filename)
        end
        
        function value = brukerDbID(obj)
            value = obj.brukerDB_id;
        end
        
        
    end
    
    methods (Access = private)
        
        function obj = update(obj,dataStorageFolder,brukerDB)
           
            % checks all exams for scans that match definition
           
            for exID = 1:size(obj.databaseRoot,1)
                 if ~isempty(obj.databaseRoot{exID,1})
                     [obj.databaseRoot{exID,2},tmpCell] = brukerdatacompletenesscheck(brukerDB.show(exID).location,obj.definition,false,[],dataStorageFolder);%fullfile(db.show(jDir).location,'dataInfo.mat'));
                     obj.databaseRoot{exID,3} = tmpCell{3};
                     obj.status = 'modified';
                 end
             end
        end
        
        function obj = load(obj,filename)
            if isempty(filename)
                error('Must supply a database location.')
            elseif ischar(filename) && exist(filename,'file')
                loadedDb = load(filename);
                obj.databaseRoot = loadedDb.dbVar;
                obj.brukerDB_id  = loadedDb.dbID;
                obj.definition  = loadedDb.dbDefinition;
                
                if obj.dbVersion ~= loadedDb.DB_VERSION
                    error('DB version incompatible.')
                end
                obj.status = 'original';
            else
                obj.databaseRoot = [];
                obj.brukerDB_id = [];
                obj.status = 'new';
            end
            obj.dbLocation = filename;
        end
        
        
    
end

end