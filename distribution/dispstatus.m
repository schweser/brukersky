function dispstatus(string,Options,type,isReset)

%DISPSTATUS Function for displaying status information.
%
%   Syntax
%
%   DISPSTATUS(s)
%   DISPSTATUS(s,Options)
%   DISPSTATUS(s,[],type)
%   DISPSTATUS(s,[],type,true)
%
%
%
%   Description
%
%   DISPSTATUS(s) displays the string s as >status< message.
%
%   DISPSTATUS(s,Options) allows specification of additional parameters.
%   The following fields are currently supported:
%       displayLevel - Specification of the level of status information to be
%                      shown. The following levels are currently supported:
%                          0  - no output
%                          1  - functioncall/functionexit only
%                          2  - additional status information
%                          3  - additional iteration information
%                          6  - shell commands
%                          10 - verbose (default)
%
%   DISPSTATUS(s,[],type) specification of the message type. The following
%   types are currently supported:
%       'functioncall'  - inidaction that a function is called
%       'functionexit'  - inidaction that a function is ended
%       'status'        - normal status message (default)
%       'iteration'     - information about iterations/sub-steps
%       'unix'          - unix shell commands
%       'structure'     - information about all fields and values of struct
%       'warning'       - warning status message (red)
%       'failed'        - failed status message (red + 'failed')
%       'passed'        - ok status message (green + 'passed')
%       'unknown'       - unknown status message (+ 'unknown')
%
%   DISPSTATUS(s,[],type,true) - resets the function counter. This should
%   be used the first time this function is called.

% F Schweser - mail@ferdinand-schweser.de - 2011/03/10
%
%   Changelog:
%
%   v1.0 - 2011/03/10 - F Schweser - initial version
%   v1.1 - 2011/03/18 - A Deistung - structure types are now supported
%   v1.2 - 2011/03/10 - F Schweser - Bugfix: abortion handling
%   v1.3 - 2011/03/14 - F Schweser - isReset added + unix mode added
%   v1.4 - 2011/05/06 - A Deistung - BugFix: for interpreting logical structure fields
%   v1.5 - 2011/05/21 - A Deistung - BugFix: nested structures and cells
%   v1.6 - 2011/09/39 - F Schweser - Bugfix: cell supplied
%   v1.7 - 2012/02/01 - F Schweser - Bugfix of default diplay level +
%                                    Minor improvement of design
%   v1.8 - 2012/08/08 - F Schweser - Now supports cprintf
%   v1.9 - 2012/08/24 - F Schweser - New type "warning" + "passed" + 'failed'
%   v1.91 - 2012/08/30 - F Schweser - Minor bugfix for cluster.
%   v2.0 - 2012/09/04 - F Schweser - New mode 'unknown'
%   v2.1 - 2013/11/05 - F Schweser - Bugfix: displayLevel 0
%   v2.2 - 2013/11/19 - A Deistung - Bugfix: if input field is object
%   v2.3 - 2014/03/17 - F Schweser - Removed color function because the
%                                    embedded 3rd party function does not
%                                    work any more on R2013. 


if nargin < 2 || ~isfield(Options,'displayLevel') || isempty(Options.displayLevel)
    Options.displayLevel = 10;
end

if nargin < 3 || isempty(type)
    if isstruct(string)
        type = 'structure';
    else
        type = 'status';
    end
end
if nargin < 4 || isempty(isReset)
    isReset = false;
end


persistent displaySubLevel
persistent functionWasAborted

if isempty(displaySubLevel)
    displaySubLevel = 0;
end

if isempty(functionWasAborted) || functionWasAborted || isReset
    displaySubLevel = 0;
end
functionWasAborted = true;

subLevelSpacer = '   ';
lineBreak              = char(13);

% randomstrings hello
firstStr{1} = 'Hi, my name is ';
secondStr{1} = 'How are you today?';
finalStr{1} = 'Have a pleasant day!';
firstStr{2} = 'Hey dude, I''m ';
secondStr{2} = 'What''s up?';
finalStr{2} = 'See ya!';
firstStr{3} = 'Hi, this is ';
secondStr{3} = '';
finalStr{3} = 'Goodbye.';
firstStr{4} = 'May I introduce myself? ';
secondStr{4} = 'Let''s do some work!';
finalStr{4} = 'That was great!';
firstStr{5} = 'I''m ';
secondStr{5} = 'How is it going?';
finalStr{5} = 'Bye bye.';


if Options.displayLevel
    switch type
        case 'functioncall'
            displaySubLevel = displaySubLevel + 1;
            displaySubLevelNew = displaySubLevel;
            
            strNo = randi(5);
            dispStr = ['********** ' firstStr{strNo} ,string,'! ' secondStr{strNo} ' **********'];
            styleStr = 'Blue';
        case 'functionexit'
            strNo = randi(5);
            dispStr = ['********** ' finalStr{strNo} ' **********'];
            displaySubLevelNew = displaySubLevel - 1;
            styleStr = 'Blue';
        case 'status'
            if Options.displayLevel > 1
                if iscell(string)
                    string = 'cell';
                end
                dispStr = [' - ' string];
            end
            displaySubLevelNew = displaySubLevel;
            styleStr = 'Black';
        case 'warning'
            if Options.displayLevel > 1
                if iscell(string)
                    string = 'cell';
                end
                dispStr = [' - ' string];
            end
            displaySubLevelNew = displaySubLevel;
            styleStr = 'Red';
        case 'passed'
            if Options.displayLevel > 1
                if iscell(string)
                    string = 'cell';
                end
                dispStr = [' - ' string ': passed'];
            end
            displaySubLevelNew = displaySubLevel;
            styleStr = 'Green';
        case 'unknown'
            if Options.displayLevel > 1
                if iscell(string)
                    string = 'cell';
                end
                dispStr = [' - ' string ': unknown'];
            end
            displaySubLevelNew = displaySubLevel;
            styleStr = 'Back';
        case 'failed'
            if Options.displayLevel > 1
                if iscell(string)
                    string = 'cell';
                end
                dispStr = [' - ' string ': failed'];
            end
            displaySubLevelNew = displaySubLevel;
            styleStr = 'Red';
        case 'iteration'
            if Options.displayLevel > 2
                dispStr = [' -- ',string];
            end
            displaySubLevelNew = displaySubLevel;
            styleStr = [];%[1,0.5,0];
        case 'unix'
            if Options.displayLevel > 5
                dispStr = [' - UNIX: ' string];
            end
            displaySubLevelNew = displaySubLevel;
            styleStr = 'comment';
        case 'structure'
            structureFieldNames    = fieldnames(string);
            numberOfStrctureFields = length(structureFieldNames);
            lengthLongestFieldName = size(char(structureFieldNames), 2);
            noSpacesAllFields     = 7;
            styleStr = [1,0,1];
            dispStr = [' - ', 'The structure >> ', inputname(1), ' << has the following fields:', lineBreak];
            for iFields = 1:numberOfStrctureFields
                fieldValue = getfield(string, char(structureFieldNames(iFields)));
                numberOfSpacing = lengthLongestFieldName - size(char(structureFieldNames(iFields)), 2);
                structureFieldSpacer = repmat(char(32), [1, (numberOfSpacing + noSpacesAllFields)]);
                if isnumeric(fieldValue)
                    if(ndims(fieldValue) == 2) && (numel(fieldValue) < 10)
                        valueStr = num2str(fieldValue);
                    else
                        valueStr = [num2str(size(fieldValue)), 'dimensions'];
                    end
                elseif  islogical(fieldValue)
                    if(ndims(fieldValue) == 2) && (numel(fieldValue) < 10)
                        valueStr = num2str(fieldValue);
                    else
                        valueStr = [num2str(size(fieldValue)), 'dimensions'];
                    end
                elseif isstruct(fieldValue)
                    valueStr = 'struct';
                elseif iscell(fieldValue)
                    valueStr = 'cell';
                elseif isobject(fieldValue)
                    valueStr = class(fieldValue);
                else
                    valueStr = num2str(fieldValue);
                end
                if Options.displayLevel > 1
                    dispStr =  [dispStr, '   * ', char(structureFieldNames(iFields)), ':', structureFieldSpacer, valueStr, lineBreak];
                end
            end
            displaySubLevelNew = displaySubLevel;
        otherwise
            error('This type is durrently not supported.')
            
    end
end


if displaySubLevel >= 0 && Options.displayLevel
    for jSubLevel = 2:displaySubLevel
        dispStr = [subLevelSpacer dispStr];
        detectedLineBreaks = strfind(dispStr, lineBreak);
        if ~isempty(detectedLineBreaks)
            tmp = dispStr(1:detectedLineBreaks(1));
            for iLine = 2:length(detectedLineBreaks);
                tmp = [tmp, subLevelSpacer, dispStr(detectedLineBreaks(iLine-1)+1:detectedLineBreaks(iLine))];
            end
            dispStr = tmp;
        end
    end
    displaySubLevel = displaySubLevelNew;
    try
        dispc(dispStr,styleStr);
    catch % dispc does not run on cluster...
        disp(dispStr)
    end
    
    
elseif displaySubLevel < 0
    displaySubLevel = 0;
end

if functionWasAborted
    functionWasAborted = false;
end




%% member functions (copied from C Ros, 07/2012)


    function dispc(X, color)
        %DISPC Display array wrapper function.
        %   DISPC(X) displays the array, without printing the array name.  In
        %   all other ways it's the same as leaving the semicolon off an
        %   expression except that empty arrays don't display.
        %   This is an override disp function that supports colored output.
        %   If color is given CPRINTF is used (see CPRINTF help for color options).
        %   If color is omitted the builtin DISP function is called.
        %
        %   See also DISP, CPRINTF.
        
        %   Copyright 2012 C. Ros
        %   $Revision: 1.0.0 $  $Date: 2012/07/05 10:00:00 $
        
        if not(exist('color', 'var')) || isempty(color)
            builtin('disp', [X]);
        else
            builtin('disp', [X]);
            %cprintf(color, [X '\n']);
        end
        
    end
    function count = cprintf(style,format,varargin)
        % CPRINTF displays styled formatted text in the Command Window
        %
        % Syntax:
        %    count = cprintf(style,format,...)
        %
        % Description:
        %    CPRINTF processes the specified text using the exact same FORMAT
        %    arguments accepted by the built-in SPRINTF and FPRINTF functions.
        %
        %    CPRINTF then displays the text in the Command Window using the
        %    specified STYLE argument. The accepted styles are those used for
        %    Matlab's syntax highlighting (see: File / Preferences / Colors /
        %    M-file Syntax Highlighting Colors), and also user-defined colors.
        %
        %    The possible pre-defined STYLE names are:
        %
        %       'Text'                 - default: black
        %       'Keywords'             - default: blue
        %       'Comments'             - default: green
        %       'Strings'              - default: purple
        %       'UnterminatedStrings'  - default: dark red
        %       'SystemCommands'       - default: orange
        %       'Errors'               - default: light red
        %       'Hyperlinks'           - default: underlined blue
        %
        %       'Black','Cyan','Magenta','Blue','Green','Red','Yellow','White'
        %
        %    Note: styles beginning with '-' will be underlined. For example:
        %          '-Blue' is underlined blue, like 'Hyperlinks';
        %          '-Comments' is underlined green etc.
        %
        %    STYLE also accepts a regular Matlab RGB vector, that can be negated
        %    for underlining. For example: -[0,1,1] means underlined cyan.
        %
        %    STYLE is case-insensitive and accepts unique partial strings just
        %    like handle property names.
        %
        %    CPRINTF by itself, without any input parameters, displays a demo
        %
        % Example:
        %    cprintf;   % displays the demo
        %    cprintf('text',   'regular black text');
        %    cprintf('hyper',  'followed %s','by');
        %    cprintf('k',      '%d colored', 4);
        %    cprintf('-comment','& underlined');
        %    cprintf('err',    'elements\n');
        %    cprintf('cyan',   'cyan');
        %    cprintf('-green', 'underlined green');
        %    cprintf(-[1,0,1], 'underlined magenta');
        %    cprintf([1,0.5,0],'and multi-\nline orange\n');
        %    cprintf('string');  % same as fprintf('string') and cprintf('text','string')
        %
        % Bugs and suggestions:
        %    Please send to Yair Altman (altmany at gmail dot com)
        %
        % Warning:
        %    This code heavily relies on undocumented and unsupported Matlab
        %    functionality. It works on Matlab 7+, but use at your own risk!
        %
        %    A technical description of the implementation can be found at:
        %    <a href="http://undocumentedmatlab.com/blog/cprintf/">http://UndocumentedMatlab.com/blog/cprintf/</a>
        %
        % Limitations:
        %    1. In R2011a and earlier, a single space char is inserted at the
        %       beginning of each CPRINTF text segment (this is ok in R2011b+).
        %
        %    2. In R2011a and earlier, consecutive differently-colored multi-line
        %       CPRINTFs sometimes display incorrectly on the bottom line.
        %       As far as I could tell this is due to a Matlab bug. Examples:
        %         >> cprintf('-str','under\nline'); cprintf('err','red\n'); % hidden 'red', unhidden '_'
        %         >> cprintf('str','regu\nlar'); cprintf('err','red\n'); % underline red (not purple) 'lar'
        %
        %    3. Sometimes, non newline ('\n')-terminated segments display unstyled
        %       (black) when the command prompt chevron ('>>') regains focus on the
        %       continuation of that line (I can't pinpoint when this happens).
        %       To fix this, simply newline-terminate all command-prompt messages.
        %
        %    4. In R2011b and later, the above errors appear to be fixed. However,
        %       the last character of an underlined segment is not underlined for
        %       some unknown reason (add an extra space character to make it look better)
        %
        %    5. In old Matlab versions (e.g., Matlab 7.1 R14), multi-line styles
        %       only affect the first line. Single-line styles work as expected.
        %       R14 also appends a single space after underlined segments.
        %
        % Change log:
        %    2011-11-27: Fixes for R2011b
        %    2011-08-29: Fix by Danilo (FEX comment) for non-default text colors
        %    2011-03-04: Performance improvement
        %    2010-06-27: Fix for R2010a/b; fixed edge case reported by Sharron; CPRINTF with no args runs the demo
        %    2009-09-28: Fixed edge-case problem reported by Swagat K
        %    2009-05-28: corrected nargout behavior sugegsted by Andreas G�b
        %    2009-05-13: First version posted on <a href="http://www.mathworks.com/matlabcentral/fileexchange/authors/27420">MathWorks File Exchange</a>
        %
        % See also:
        %    sprintf, fprintf
        
        % License to use and modify this code is granted freely to all interested, as long as the original author is
        % referenced and attributed as such. The original author maintains the right to be solely associated with this work.
        
        % Programmed and Copyright by Yair M. Altman: altmany(at)gmail.com
        % $Revision: 1.05 $  $Date: 2011/11/27 21:06:28 $
        
        persistent majorVersion minorVersion
        if isempty(majorVersion)
            %v = version; if str2double(v(1:3)) <= 7.1
            %majorVersion = str2double(regexprep(version,'^(\d+).*','$1'));
            %minorVersion = str2double(regexprep(version,'^\d+\.(\d+).*','$1'));
            %[a,b,c,d,versionIdStrs]=regexp(version,'^(\d+)\.(\d+).*');  %#ok unused
            v = sscanf(version, '%d.', 2);
            majorVersion = v(1); %str2double(versionIdStrs{1}{1});
            minorVersion = v(2); %str2double(versionIdStrs{1}{2});
        end
        
        % The following is for debug use only:
        %global docElement txt el
        if ~exist('el','var') || isempty(el),  el=handle([]);  end  %#ok mlint short-circuit error ("used before defined")
        if nargin<1, showDemo(majorVersion,minorVersion); return;  end
        if isempty(style),  return;  end
        if all(ishandle(style)) && length(style)~=3
            dumpElement(style);
            return;
        end
        
        % Process the text string
        if nargin<2, format = style; style='text';  end
        %error(nargchk(2, inf, nargin, 'struct'));
        %str = sprintf(format,varargin{:});
        
        % Get the normalized style name and underlining flag
        [underlineFlag, style] = processStyleInfo(style);
        
        % Set hyperlinking, if so requested
        if underlineFlag
            format = ['<a href="">' format '</a>'];
            
            % Matlab 7.1 R14 (possibly a few newer versions as well?)
            % have a bug in rendering consecutive hyperlinks
            % This is fixed by appending a single non-linked space
            if majorVersion < 7 || minorVersion <= 1
                format(end+1) = ' ';
            end
        end
        
        % Get the current CW position
        cmdWinDoc = com.mathworks.mde.cmdwin.CmdWinDocument.getInstance;
        lastPos = cmdWinDoc.getLength;
        
        % If not beginning of line
        bolFlag = 0;  %#ok
        %if docElement.getEndOffset - docElement.getStartOffset > 1
        % Display a hyperlink element in order to force element separation
        % (otherwise adjacent elements on the same line will be merged)
        if majorVersion<7 || minorVersion<13
            if ~underlineFlag
                fprintf('<a href=""> </a>');  %fprintf('<a href=""> </a>\b');
            else
                fprintf(' ');  %fprintf(' \b');
            end
        end
        %drawnow;
        bolFlag = 1;
        %end
        
        % Get a handle to the Command Window component
        mde = com.mathworks.mde.desk.MLDesktop.getInstance;
        cw = mde.getClient('Command Window');
        xCmdWndView = cw.getComponent(0).getViewport.getComponent(0);
        
        % Store the CW background color as a special color pref
        % This way, if the CW bg color changes (via File/Preferences),
        % it will also affect existing rendered strs
        com.mathworks.services.Prefs.setColorPref('CW_BG_Color',xCmdWndView.getBackground);
        
        % Display the text in the Command Window
        count1 = fprintf(2,format,varargin{:});
        %awtinvoke(cmdWinDoc,'remove',lastPos,1);   % TODO: find out how to remove the extra '_'
        drawnow;
        docElement = cmdWinDoc.getParagraphElement(lastPos+1);
%         if majorVersion<7 || minorVersion<13
%             if bolFlag && ~underlineFlag
%                 % Set the leading hyperlink space character ('_') to the bg color, effectively hiding it
%                 % Note: old Matlab versions have a bug in hyperlinks that need to be accounted for...
%                 %disp(' '); dumpElement(docElement)
%                 setElementStyle(docElement,'CW_BG_Color',1+underlineFlag,majorVersion,minorVersion); %+getUrlsFix(docElement));
%                 %disp(' '); dumpElement(docElement)
%                 el(end+1) = handle(docElement);
%             end
%             
%             % Fix a problem with some hidden hyperlinks becoming unhidden...
%             fixHyperlink(docElement);
%             %dumpElement(docElement);
%         end
        
        % Get the Document Element(s) corresponding to the latest fprintf operation
        while docElement.getStartOffset < cmdWinDoc.getLength
            % Set the element style according to the current style
            setElementStyle(docElement,style,underlineFlag,majorVersion,minorVersion);
            docElement2 = cmdWinDoc.getParagraphElement(docElement.getEndOffset+1);
            if isequal(docElement,docElement2),  break;  end
            docElement = docElement2;
        end
        
        % Force a Command-Window repaint
        % Note: this is important in case the rendered str was not '\n'-terminated
        xCmdWndView.repaint;
        
        % The following is for debug use only:
        el(end+1) = handle(docElement);  %#ok used in debug only
        %elementStart = docElement.getStartOffset;
        %elementLength = docElement.getEndOffset - elementStart;
        %txt = cmdWinDoc.getText(elementStart,elementLength);
        
        if nargout
            count = count1;
        end
        return;  % debug breakpoint
    end
% Process the requested style information
    function [underlineFlag,style] = processStyleInfo(style)
        underlineFlag = 0;
        
        % Style = valid matlab RGB vector
        if isnumeric(style) && length(style)==3 && all(style<=1) && all(abs(style)>=0)
            if any(style<0)
                underlineFlag = 1;
                style = abs(style);
            end
            style = getColorStyle(style);
            
        elseif ~ischar(style)
            error('YMA:cprintf:InvalidStyle','Invalid style - see help section for a list of valid style values')
            
            % Style name
        else
            % Styles starting with '-' should be underlined (using a no-target hyperlink hack)
            if style(1)=='-'
                underlineFlag = 1;
                style = style(2:end);
            end
            
            % Try case-insensitive partial/full match with the accepted style names
            validStyles = {'Text','Keywords','Comments','Strings','UnterminatedStrings','SystemCommands','Errors', ...
                'Black','Cyan','Magenta','Blue','Green','Red','Yellow','White', ...
                'Hyperlinks'};
            matches = find(strncmpi(style,validStyles,length(style)));
            
            % No match - error
            if isempty(matches)
                error('YMA:cprintf:InvalidStyle','Invalid style - see help section for a list of valid style values')
                
                % Too many matches (ambiguous) - error
            elseif length(matches) > 1
                error('YMA:cprintf:AmbigStyle','Ambiguous style name - supply extra characters for uniqueness')
                
                % Regular text
            elseif matches == 1
                style = 'ColorsText';  % fixed by Danilo, 29/8/2011
                
                % Highlight preference style name
            elseif matches < 8
                style = ['Colors_M_' validStyles{matches}];
                
                % Color name
            elseif matches < length(validStyles)
                colors = [0,0,0; 0,1,1; 1,0,1; 0,0,1; 0,1,0; 1,0,0; 1,1,0; 1,1,1];
                requestedColor = colors(matches-7,:);
                style = getColorStyle(requestedColor);
                
                % Hyperlink
            else
                style = 'Colors_HTML_HTMLLinks';  % CWLink
                underlineFlag = 1;
            end
        end
    end
% Convert a Matlab RGB vector into a known style name (e.g., '[255,37,0]')
    function styleName = getColorStyle(rgb)
        intColor = int32(rgb*255);
        javaColor = java.awt.Color(intColor(1), intColor(2), intColor(3));
        styleName = sprintf('[%d,%d,%d]',intColor);
        com.mathworks.services.Prefs.setColorPref(styleName,javaColor);
    end
%  com.mathworks.services.FontPrefs.setCodeFont(java.awt.Font('Monospaced',java.awt.Font.BOLD,12))
% Fix a bug in some Matlab versions, where the number of URL segments
% is larger than the number of style segments in a doc element
    function delta = getUrlsFix(docElement)  %#ok currently unused
        tokens = docElement.getAttribute('SyntaxTokens');
        links  = docElement.getAttribute('LinkStartTokens');
        if length(links) > length(tokens(1))
            delta = length(links) > length(tokens(1));
        else
            delta = 0;
        end
    end
% fprintf(2,str) causes all previous '_'s in the line to become red - fix this
    function fixHyperlink(docElement)
        try
            tokens = docElement.getAttribute('SyntaxTokens');
            urls   = docElement.getAttribute('HtmlLink');
            urls   = urls(2);
            links  = docElement.getAttribute('LinkStartTokens');
            offsets = tokens(1);
            styles  = tokens(2);
            doc = docElement.getDocument;
            
            % Loop over all segments in this docElement
            for idx = 1 : length(offsets)-1
                % If this is a hyperlink with no URL target and starts with ' ' and is collored as an error (red)...
                if strcmp(styles(idx).char,'Colors_M_Errors')
                    character = char(doc.getText(offsets(idx)+docElement.getStartOffset,1));
                    if strcmp(character,' ')
                        if isempty(urls(idx)) && links(idx)==0
                            % Revert the style color to the CW background color (i.e., hide it!)
                            styles(idx) = java.lang.String('CW_BG_Color');
                        end
                    end
                end
            end
        catch
            % never mind...
        end
    end
% Set an element to a particular style (color)
    function setElementStyle(docElement,style,hyperlinkFlag, majorVersion,minorVersion)
        %global tokens links urls urlTargets  % for debug only
        global oldStyles
        if nargin<3,  hyperlinkFlag=0;  end
        % Set the last Element token to the requested style:
        % Colors:
        tokens = docElement.getAttribute('SyntaxTokens');
        try
            styles = tokens(2);
            oldStyles{end+1} = styles.cell;
            
            % Correct edge case problem
            extraInd = double(majorVersion>=7 && minorVersion>=13);  % =0 for R2011a-, =1 for R2011b+
            %{
      if ~strcmp('CWLink',char(styles(end-hyperlinkFlag))) && ...
          strcmp('CWLink',char(styles(end-hyperlinkFlag-1)))
         extraInd = 0;%1;
      end
            %}
            %hyperlinkFlag = ~isempty(strmatch('CWLink',tokens(2)));
            %hyperlinkFlag = 0 + any(cellfun(@(c)(~isempty(c)&&strcmp(c,'CWLink')),tokens(2).cell));
            
            styles(end-extraInd) = java.lang.String('');
            styles(end-extraInd-hyperlinkFlag) = java.lang.String(style);  %#ok apparently unused but in reality used by Java
            if extraInd
                styles(end-hyperlinkFlag) = java.lang.String(style);
            end
            
            oldStyles{end} = [oldStyles{end} styles.cell];
        catch
            % never mind for now
        end
        
        % Underlines (hyperlinks):
        links = docElement.getAttribute('LinkStartTokens');
        if isempty(links)
            %docElement.addAttribute('LinkStartTokens',repmat(int32(-1),length(tokens(2)),1));
        else
            %TODO: remove hyperlink by setting the value to -1
        end
        
        % Correct empty URLs to be un-hyperlinkable (only underlined)
        urls  = docElement.getAttribute('HtmlLink');
        if isempty(urls),  return;  end
        urlTargets = urls(2);
        for urlIdx = 1 : length(urlTargets)
            try
                if urlTargets(urlIdx).length < 1
                    urlTargets(urlIdx) = [];  % '' => []
                end
            catch
                % never mind...
                a=1;  %#ok used for debug breakpoint...
            end
        end
    end
% Display information about element(s)
    function dumpElement(docElements)
        %return;
        numElements = length(docElements);
        cmdWinDoc = docElements(1).getDocument;
        for elementIdx = 1 : numElements
            if numElements > 1,  fprintf('Element #%d:\n',elementIdx);  end
            docElement = docElements(elementIdx);
            if ~isjava(docElement),  docElement = docElement.java;  end
            %docElement.dump(java.lang.System.out,1)
            tokens = docElement.getAttribute('SyntaxTokens');
            if isempty(tokens),  continue;  end
            links = docElement.getAttribute('LinkStartTokens');
            urls  = docElement.getAttribute('HtmlLink');
            txt = {};
            tokenLengths = tokens(1);
            for tokenIdx = 1 : length(tokenLengths)-1
                tokenLength = diff(tokenLengths(tokenIdx+[0,1]));
                if (tokenLength < 0)
                    tokenLength = docElement.getEndOffset - docElement.getStartOffset - tokenLengths(tokenIdx);
                end
                txt{tokenIdx} = cmdWinDoc.getText(docElement.getStartOffset+tokenLengths(tokenIdx),tokenLength).char;  %#ok
            end
            lastTokenStartOffset = docElement.getStartOffset + tokenLengths(end);
            txt{end+1} = cmdWinDoc.getText(lastTokenStartOffset, docElement.getEndOffset-lastTokenStartOffset).char;  %#ok
            %cmdWinDoc.uiinspect
            %docElement.uiinspect
            txt = strrep(txt',sprintf('\n'),'\n');
            try
                data = [tokens(2).cell m2c(tokens(1)) m2c(links) m2c(urls(1)) cell(urls(2)) txt];
            catch
                try
                    data = [tokens(2).cell m2c(tokens(1)) m2c(links) txt];
                catch
                    disp([tokens(2).cell m2c(tokens(1)) txt]);
                    try
                        data = [m2c(links) m2c(urls(1)) cell(urls(2))];
                    catch
                        % Mtlab 7.1 only has urls(1)...
                        data = [m2c(links) urls.cell];
                    end
                end
            end
            disp(' ');
            disp(docElement)
            disp(data)
        end
    end
% Utility function to convert matrix => cell
    function cells = m2c(data)
        %datasize = size(data);  cells = mat2cell(data,ones(1,datasize(1)),ones(1,datasize(2)));
        cells = num2cell(data);
    end

% Display the help and demo
    function showDemo(majorVersion,minorVersion)
        fprintf('cprintf displays formatted text in the Command Window.\n\n');
        fprintf('Syntax: count = cprintf(style,format,...);  click <a href="matlab:help cprintf">here</a> for details.\n\n');
        fprintf('Demo:\n\n');
        s = ['cprintf(''text'',    ''regular black text'');' 10 ...
            'cprintf(''hyper'',   ''followed %s'',''by'');' 10 ...
            'cprintf(''k'',       ''%d colored'',4);' 10 ...
            'cprintf(''-comment'',''& underlined'');' 10 ...
            'cprintf(''err'',     ''elements:\n'');' 10 ...
            'cprintf(''cyan'',    ''cyan'');' 10 ...
            'cprintf(''-green'',  ''underlined green'');' 10 ...
            'cprintf(-[1,0,1],  ''underlined magenta'');' 10 ...
            'cprintf([1,0.5,0], ''and multi-\nline orange\n'');' 10];
        if majorVersion>=7 && minorVersion>=13
            % In R2011b+ the internal bug that causes the need for an extra space
            % is apparently fixed, so we must insert the sparator spaces manually...
            s = strrep(s, ''')',' '')');
            s = strrep(s, '\n ','\n');
        end
        disp(s);
        eval(s);
    end

%%%%%%%%%%%%%%%%%%%%%%%%%% TODO %%%%%%%%%%%%%%%%%%%%%%%%%
% - Fix: Remove leading space char (hidden underline '_')
% - Fix: Find workaround for multi-line quirks/limitations
% - Fix: Non-\n-terminated segments are displayed as black
% - Fix: Check whether the hyperlink fix for 7.1 is also needed on 7.2 etc.
% - Enh: Add font support








end