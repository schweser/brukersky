function [found,foundCell] = brukerdatacompletenesscheck(directory,infoExpected,isUseScanFolderNames,resultFilename,baseFolder)

%BRUKERDATACOMPLETENESSCHECK checks Bruker data folders for completeness
%
% Syntax
%
%   A = BRUKERDATACOMPLETENESSCHECK(directory,infoExpected)
%   A = BRUKERDATACOMPLETENESSCHECK(directory,infoExpected,true)
%   A = BRUKERDATACOMPLETENESSCHECK(directory,infoExpected,[],filename)
%   A = BRUKERDATACOMPLETENESSCHECK(directory,infoExpected,[],[],baseFolder)
%
%
% Description
%
%   [A,B] = BRUKERDATACOMPLETENESSCHECK(directory,infoExpected) checks all scan
%   folders in directory for completeness according to the information
%   specified in vector-struct infoExpected. A contains numerical
%   information on how many mathing sequences were found. B contains
%   detailed information where to find these datasets.
%   Each  element of the vector
%   infoExpected corresponds to one sequence name. The following checks are
%   supported:
%
%       ACQ_scan_name      - name of scan, e.g. '05r_03_STEAM'
%       raw.fileSize       - file size of raw file in bytes
%       raw.fileList       - cell with strings of filenames to be supposed
%                            in the raw folder, e.g. {'acqp','AdjStatePerScan','fid','method','pulseprogram','spnam0','spnam12','spnam26','uxnmr.par'}
%       raw.noOfFiles      - minimum number of files in the raw folder
%       reco.fileList      - cell with strings of filenames to be supposed
%                            in the reco folder
%       reco.noOfFiles     - minimum number of files in the reco folder
%       
%   [A,B] = BRUKERDATACOMPLETENESSCHECK(directory,infoExpected,true) uses the folder names of the
%   scans to identify scan name. Use this only if data was previously
%   sorted by SORTBRUKERRAWDATA.
%
%   [A,B] = BRUKERDATACOMPLETENESSCHECK(directory,infoExpected,[],filename)
%   stores the advanced information B in filename. If this option is used,
%   the function loads filename in the beginning and skips studies that
%   have already been analyzed earlier.
%
%   [A,B] =
%   BRUKERDATACOMPLETENESSCHECK(directory,infoExpected,[],[],baseFolder)
%   Use this to return only relative paths, relative to a base folder.
%
%
% Example
%         expected{1}.ACQ_scan_name = '04_01_MGE';
%         expected{1}.raw.fileSize = 663552000;
%         expected{1}.raw.fileList = {'acqp','AdjStatePerScan','fid','method','pulseprogram','spnam0','spnam26','uxnmr.par'};
%         expected{2}.ACQ_scan_name = '05r_03_STEAM';
%         expected{2}.raw.fileSize = 32768;
%         expected{2}.raw.fileList = {'acqp','acqu','acqus','AdjStatePerScan','fid','fid.refscan','method','pulseprogram','spnam1','spnam2','spnam3','spnam4','spnam5','uxnmr.par'};
%         expected{3}.ACQ_scan_name = '06_02_2DEPI_DTI';
%         expected{3}.raw.fileSize = 190836800;
%         expected{3}.reco.fileList = {'2dseq','d3proc','id','meta','procs','reco','roi','visu_pars'};
%         expected{3}.raw.fileList = {'acqp','AdjStatePerScan','fid','method','pulseprogram','spnam0','spnam12','spnam26','uxnmr.par'};
%         datasetsFound = brukerdatacompletenesscheck(directory,expected,true);

% F Schweseer -  27/04/2015
%
%   Changelog:
%
%   2015-04-27 - v1.0 - FSchweser - initial
%   2015-04-28 - v1.1 - FSchweser - added file option
%   2015-07-23 - v1.2 - FSchweser - added baseFolder
%   2015-09-28 - v1.3 - FSchweser - now catches invalid strings in sequence name
%   2015-09-30 - v1.4 - FSchweser - Now accepts afterDescr without after
%   2015-11-23 - v1.5 - FSchweser/NBertolino - Bugfix, now considers real
%                       acquisition time for after/before conditions
%   2016-06-15 - v1.6 - FSchweser - We are now using (acqp) protocol name
%                                   instead of scan_name, because in PV6 scan name includes the run number

if nargin < 4 || isempty(resultFilename)
    isUseResultFile = false;
    jFoundCell = 0;
    foundCell = cell(1,3);
else
    isUseResultFile = true;
    if exist(resultFilename,'file')
        load(resultFilename)
        jFoundCell = size(foundCell,1); %#ok<NODEF>
    else
        jFoundCell = 0;
        foundCell = cell(1,3);
    end
end

if nargin < 5 || isempty(baseFolder)
    baseFolder = '';
end
directory = fullfile(baseFolder,directory);


if exist(fullfile(directory,'subject'),'file')
    directoryList{1,1} = ''; % this is a study folder
    isStudyFolder = true;
else
    directoryList = dirc(directory,'de');
    isStudyFolder = false;
end


found = [];

for jDir = 1:size(directoryList,1)
    
    
    studyFolder = fullfile(directory,directoryList{jDir,1});
    
    if isempty(foundCell{1,1}) || isempty(find(not(cellfun('isempty', strfind(foundCell(:,1),studyFolder)))))
        
        jFoundCell = jFoundCell + 1;
        dispstatus(['Scanning ' studyFolder])

        scanFolders = dirc(studyFolder,'de');
        ParameterListSubject = readBrukerParamFile(fullfile(studyFolder,'subject'));

        for jExpected = 1:size(infoExpected,2)
            
            if myisfield(infoExpected{jExpected},'afterDescr') 
                afterStr{jExpected} = ['___' strrep(strrep(infoExpected{jExpected}.afterDescr,'-','_'),' ','_')];
            else
                afterStr{jExpected} = '';
            end
            
            if isfield(ParameterListSubject,'SUBJECT_id')
                if isStudyFolder
                    found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]) = 0;
                else
                    found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]).(['id_' strrep(ParameterListSubject.SUBJECT_id,'-','_')]) = 0;
                end
            end
        end
        foundCell{jFoundCell,1} = studyFolder;
        foundCell{jFoundCell,2} = ParameterListSubject.SUBJECT_id;
        foundCell{jFoundCell,3} = [];
        previousScanRequirementFound = cell(size(infoExpected,2),1);
        beforeScanRequirementFound = cell(size(infoExpected,2),1);
        
        
        
        clear ParameterListScanAcqp date_dayFormatted isAcqpFileReadSuccess
        if ~isUseScanFolderNames
            eliminateThisCell = false(size(scanFolders,1),1);
            isAcqpFileReadSuccess = cell(size(scanFolders,1),1);
            date_dayFormatted = cell(size(scanFolders,1),1);
            ParameterListScanAcqp = cell(size(scanFolders,1),1);
            for jScan = 1:size(scanFolders,1)
                
                scanFolder = fullfile(directory,directoryList{jDir,1},scanFolders{jScan,1});
                acqpFile = fullfile(scanFolder,'acqp');
                if exist(acqpFile,'file')
                    
                    try
                        [ParameterListScanAcqp{jScan},fileHeader] = readBrukerParamFile(acqpFile); % this one is time consuming
                        fileHeader = char(fileHeader{6,2});
                        fileHeader = fileHeader(5:24);
                        date_numMatlab = datenum(fileHeader,'mmm dd HH:MM:SS yyyy');
                        
                        % we do the following because the time stamp is
                        % changed whenever a scan is touched. We want to
                        % avoid that a scan is queued but never acquired
                        % and can then be identified by the before and
                        % after classifications.
                        if ~isfield(ParameterListScanAcqp{jScan},'ACQ_completed') || ~strcmp(ParameterListScanAcqp{jScan}.ACQ_completed,'Yes')
                            date_dayFormatted{jScan} = 'zzzzzzzzzzzzzzzzzzzz'; 
                            eliminateThisCell(jScan) = true;
                        else
                            % scan was acquired
                            date_dayFormatted{jScan} = datestr(date_numMatlab,'yyyymmddHHMMSS');
                        end
                        
                        isAcqpFileReadSuccess{jScan} = true;
                    catch
                        isAcqpFileReadSuccess{jScan} = false;
                    end
                else
                    date_dayFormatted{jScan} = 'zzzzzzzzzzzzzzzzzzzz'; 
                    eliminateThisCell(jScan) = true;
                end
            end
        end
        [~,acqOrderScans] = sort(date_dayFormatted);
        eliminateThisCell = eliminateThisCell(acqOrderScans);
        acqOrderScans = acqOrderScans(~eliminateThisCell);
        
        for jScan = acqOrderScans'

            scanFolder = fullfile(directory,directoryList{jDir,1},scanFolders{jScan,1});
            acqpFile = fullfile(scanFolder,'acqp');
            if exist(acqpFile,'file')

                fidFile = fullfile(scanFolder,'fid');
                serFile = fullfile(scanFolder,'ser');
                if exist(fidFile,'file')
                    fileInfo = dir(fidFile);
                elseif exist(serFile,'file')
                    fileInfo = dir(serFile);
                end
                
                if isAcqpFileReadSuccess{jScan} && isfield(ParameterListScanAcqp{jScan},'ACQ_completed') && ~strcmp(ParameterListScanAcqp{jScan}.ACQ_completed,'No')
                    for jExpected = 1:size(infoExpected,2)
                        
                        if isfield(ParameterListScanAcqp{jScan},'ACQ_protocol_name')
                            
                            
                            
                            if isUseScanFolderNames
                                isExpectedMatch = ~isempty(strfind(scanFolders{jScan,1},cleanString(infoExpected{jExpected}.ACQ_scan_name)));
                                if myisfield(infoExpected{jExpected},'after')
                                    error('This combination has not yet been implemented.')
                                end
                            else
                                
                                scanName = cleanString(ParameterListScanAcqp{jScan}.ACQ_protocol_name);
                                isExpectedMatch = strcmp(scanName,cleanString(infoExpected{jExpected}.ACQ_scan_name));
                                
                                if myisfield(infoExpected{jExpected},'after') && (isempty(previousScanRequirementFound{jExpected}) || previousScanRequirementFound{jExpected}==0)
                                    previousScanRequirementFound{jExpected} = strcmp(scanName,cleanString(infoExpected{jExpected}.after));
                                end
                                if myisfield(infoExpected{jExpected},'before') && (isempty(beforeScanRequirementFound{jExpected}) || beforeScanRequirementFound{jExpected}==0)
                                    beforeScanRequirementFound{jExpected} = strcmp(scanName,cleanString(infoExpected{jExpected}.before));
                                end
                                
                            end
                        end
                        if ~isfield(infoExpected{jExpected},'ACQ_scan_name') || isExpectedMatch
                            if ~myisfield(infoExpected{jExpected},'raw') || ~myisfield(infoExpected{jExpected}.raw,'fileSize') || fileInfo.bytes == infoExpected{jExpected}.raw.fileSize
                                if ~myisfield(infoExpected{jExpected},'raw') || ~myisfield(infoExpected{jExpected}.raw,'noOfFiles') || size(dirc(scanFolder,'f'),1) >= infoExpected{jExpected}.raw.noOfFiles;
                                    if ~myisfield(infoExpected{jExpected},'reco') || ~myisfield(infoExpected{jExpected}.reco,'noOfFiles') || size(dirc(fullfile(scanFolder,'pdata','1'),'f'),1) >= infoExpected{jExpected}.reco.noOfFilesReco;
                                        
                                        isFilesComplete = true;
                                        if myisfield(infoExpected{jExpected},'reco') && myisfield(infoExpected{jExpected}.reco,'fileList')
                                            for jFile = 1:size(infoExpected{jExpected}.reco.fileList,2)
                                                isFilesComplete = exist(fullfile(scanFolder,'pdata',infoExpected{jExpected}.reco.fileList{jFile}),'file');
                                            end
                                        end
                                        
                                        
                                        if myisfield(infoExpected{jExpected},'raw') && myisfield(infoExpected{jExpected}.raw,'fileList')
                                            for jFile = 1:size(infoExpected{jExpected}.raw.fileList,2)
                                                isFilesComplete = exist(fullfile(scanFolder,infoExpected{jExpected}.raw.fileList{jFile}),'file');
                                            end
                                        end
                                        
                                        isEverythingComplete = isFilesComplete;
                                        if isEverythingComplete
                                            
                                            if myisfield(infoExpected{jExpected},'after')
                                                if ~previousScanRequirementFound{jExpected}
                                                    isEverythingComplete = false;
                                                end
                                                
                                            end
                                            
                                            if myisfield(infoExpected{jExpected},'before')
                                                if beforeScanRequirementFound{jExpected}
                                                    isEverythingComplete = false;
                                                end  
                                            end 
                                                
                                        end
                                        
                                        if isEverythingComplete
                                            
                                            if isfield(foundCell{jFoundCell,3},['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}])
                                                timesThisScanFound = size(foundCell{jFoundCell,3}.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]),2);
                                            else
                                                timesThisScanFound = 0;
                                            end
                                            
                                            if isfield(infoExpected{jExpected},'chooseMultiple')
                                                multipleMatchesHandling = infoExpected{jExpected}.chooseMultiple;
                                            else
                                                multipleMatchesHandling = 'all';
                                            end
                                            switch multipleMatchesHandling
                                                
                                                case 'last' % only last match will be used
                                                    if timesThisScanFound > 1
                                                        error('Internal error. This should not happen.')
                                                    elseif timesThisScanFound == 1
                                                        foundCell{jFoundCell,3}.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]){timesThisScanFound} = scanFolder(numel(baseFolder)+1:end);
                                                    else
                                                        if isStudyFolder
                                                            found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]) = found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]) + 1;
                                                        else
                                                            found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]).(['id_' strrep(ParameterListSubject.SUBJECT_id,'-','_')]) = found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]).(['id_' strrep(ParameterListSubject.SUBJECT_id,'-','_')]) + 1;
                                                        end
                                                        foundCell{jFoundCell,3}.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]){timesThisScanFound+1} = scanFolder(numel(baseFolder)+1:end);
                                                    end
                                                    
                                                case 'first' % only first match will be used
                                                    if timesThisScanFound > 1
                                                        error('Internal error. This should not happen.')
                                                    elseif timesThisScanFound == 0
                                                        if isStudyFolder
                                                            found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]) = found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]) + 1;
                                                        else
                                                            found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]).(['id_' strrep(ParameterListSubject.SUBJECT_id,'-','_')]) = found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]).(['id_' strrep(ParameterListSubject.SUBJECT_id,'-','_')]) + 1;
                                                        end
                                                        foundCell{jFoundCell,3}.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]){timesThisScanFound+1} = scanFolder(numel(baseFolder)+1:end);
                                                    end
                                                    
                                                    
                                                case 'all'
                                                    if isStudyFolder
                                                        found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]) = found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]) + 1;
                                                    else
                                                        found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]).(['id_' strrep(ParameterListSubject.SUBJECT_id,'-','_')]) = found.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]).(['id_' strrep(ParameterListSubject.SUBJECT_id,'-','_')]) + 1;
                                                    end
                                                    foundCell{jFoundCell,3}.(['scan__' cleanString(infoExpected{jExpected}.ACQ_scan_name) afterStr{jExpected}]){timesThisScanFound+1} = scanFolder(numel(baseFolder)+1:end);
                                                    
                                                otherwise
                                                    error('chooseMultiple variant not supported (in conf file)')
                                            end
                                            
                                            
                                            
                                            
                                        end
                                    end
                                end
                                
                                %                         found.(['id_' ParameterListSubject.SUBJECT_id]).(strrep(ParameterListSubject.SUBJECT_study_name,' ','_')).(['scan_' scanName]).folder = fullfile(directory,directoryList{jDir,1},scanFolders{jScan,1});
                                %                         found{jDir,jExpected,newCellNo}.SUBJECT_id = ParameterListSubject.SUBJECT_id;
                                %                         found{jDir,jExpected,newCellNo}.SUBJECT_study_name = ParameterListSubject.SUBJECT_study_name;
                                %                         found{jDir,jExpected,newCellNo}.ACQ_scan_name = scanName;
                                %
                                %                         found{jDir,jExpected,newCellNo}.folder = fullfile(directory,directoryList{jDir,1},scanFolders{jScan,1});
                                %
                                %                         methodFile = fullfile(found{jDir,jExpected,newCellNo}.folder,'method');
                                %                         [~,ParameterListSubjectHeader] = readBrukerParamFile(methodFile);
                                %                         IndexC = strfind(ParameterListSubjectHeader, 'Date');
                                %                         Index = find(not(cellfun('isempty', IndexC)));
                                %                         found{jDir,jExpected,newCellNo}.timestamp = datenum(ParameterListSubjectHeader{Index,2}(5:24),'mmm dd HH:MM:SS yyyy'); %#ok<*FNDSB,*AGROW>
                            end
                        end
                    end
                end
            end
        end
    else
        dispstatus(['Skipping bc already analyzed ' studyFolder])
    end
end

if isUseResultFile
    save(resultFilename,'foundCell')
end

    function strOut = cleanString(inputStr)
        strOut = strrep(inputStr,'-','_');
        strOut = strrep(strOut,' ','_');
        strOut = strrep(strOut,'(','_');
        strOut = strrep(strOut,')','_');
        % make sure changes here are also made in studyDB.m and recoDB
    end

end