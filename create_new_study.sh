#!/bin/bash

BRUKERSKYBASEPATH="/shared/nonrestricted/cbi/bruker_studies"

MASTERID="$1"
NEWID="$2"

NEWSTUDYDIR=${BRUKERSKYBASEPATH}/studies/${NEWID}
mkdir -p ${NEWSTUDYDIR}/reco_data
mkdir -p ${NEWSTUDYDIR}/reco_scripts
mkdir -p ${NEWSTUDYDIR}/db
cd ${BRUKERSKYBASEPATH}/studies/${MASTERID}
cp config reco_modules ${NEWSTUDYDIR} -R

rm ${NEWSTUDYDIR}/config/animalIDs.def
touch ${NEWSTUDYDIR}/config/animalIDs.def

