#!/bin/bash

MATLABIMG=brukersky_matlab:r2018b
IMGMAC=E4:CE:8F:60:8F:0A
BASEDIR=/shared/nonrestricted/cbi/bruker_studies


docker run \
 --rm --mac-address=${IMGMAC} --entrypoint=matlab \
 -v ${BASEDIR}/system:/code:rw \
 -v ${BASEDIR}/licenses/matlab/R2018a:/usr/local/MATLAB/R2018a/licenses/:ro \
 ${MATLABIMG} \
 -softwareopengl -r "cd /code; distribute; exit"

#currDir=`pwd`
#cd ${BASEDIR}/dockerized_apps/study_update/docker_image/
#./build.sh
#cd ${currDir}

echo Done.
