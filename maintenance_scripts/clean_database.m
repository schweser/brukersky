% this script removes duplicates from the database and moves data that is
% not in db out of data storage folder
%
% F Schweser
clear
restoredefaultpath
addpath /shared/studies/nonregulated/Bruker_studies/db_repository/schweserbox/common_matlab/initialization/autocomp/
initializeswitoolbox('buffalo')
initializesendmail('ctrc-mri@bnac.net','bnac');

confFile      = '/shared/studies/nonregulated/Bruker_studies/db/conf_updateDB.conf';
conf_global     = read_configuration('global',confFile);
eval(conf_global);


db = brukerDB(databaseFile);

dataStoragePath = '/shared/studies/nonregulated/Bruker_studies/data_original/';
allFolders = dirc(dataStoragePath,'de');


notInDbCounter = 0;
notInDb = cell(1,1);
removedExamIDCounter = 0;
removedExamID = cell(1,1);
for jFolderBase = size(allFolders,1):-1:1
    if strcmp(allFolders{jFolderBase,1}(1:7),'locked_')
        currentFolder = allFolders{jFolderBase,1};
        disp('******************************')
        disp(['Now checking studies in folder ' currentFolder])
        pause(1)
        
        folderName = fullfile(dataStoragePath,currentFolder);
        folders = dirc(folderName,'de');
        
        for jFolder = 1:size(folders,1)
            [~,animalID] = fileparts(folders{jFolder,1});
            entriesFound = db.show(db.find(animalID,'animalID'));
            if ~iscell(entriesFound)
                tmp{1} = entriesFound;
                entriesFound = tmp;
            end
            folderFull = fullfile(currentFolder,folders{jFolder,1});
            examIDcurrent = db.find(folderFull,'location');
            if isempty(examIDcurrent)
                disp(['Data in ' folderFull ' is not in database!']);
                notInDbCounter = notInDbCounter + 1;
                notInDb{notInDbCounter} = folderFull;
            else
                entryInCurrentFolder = db.show(examIDcurrent);
                if isempty(entryInCurrentFolder)
                    disp(['EX' num2str(examIDcurrent) ' (' folderFull ') is a duplicate, but it has already been removed']);
                else
                    for jEntry = 1:size(entriesFound,2)
                        if isempty(entriesFound{jEntry}.instanceUID) % some older cases don't have UID yet (was added later). In this case we check all other fields
                            if ~strcmp(entriesFound{jEntry}.location,entryInCurrentFolder.location)
                                [~,scanFolderPart1,scanFolderPart2] = fileparts(entriesFound{jEntry}.location);
                                [~,scanFolderPart1_current,scanFolderPart2_current] = fileparts(entryInCurrentFolder.location);
                                scanFolder = [scanFolderPart1,scanFolderPart2];
                                scanFolderCurrent = [scanFolderPart1_current,scanFolderPart2_current];
                                if strcmp(entriesFound{jEntry}.examName,entryInCurrentFolder.examName) && ...
                                        strcmp(scanFolder,scanFolderCurrent)
                                    disp(['Removing EX' num2str(examIDcurrent) ' (' folderFull ') because it is a duplicate of ' entriesFound{jEntry}.location])
                                    db.remove(examIDcurrent);
                                    removedExamIDCounter = removedExamIDCounter + 1;
                                    removedExamID{removedExamIDCounter}.exID = examIDcurrent;
                                    removedExamID{removedExamIDCounter}.exName = entryInCurrentFolder.examName;
                                    removedExamID{removedExamIDCounter}.SUID = animalID;
                                    removedExamID{removedExamIDCounter}.raw = entryInCurrentFolder.location;
                                end
                            end
                        elseif strcmp(entriesFound{jEntry}.instanceUID,entryInCurrentFolder.instanceUID) && ...
                                ~strcmp(entriesFound{jEntry}.location,entryInCurrentFolder.location)
                            % this is a duplicate! Now disable the one in the current
                            % folder
                            disp(['Removing EX' num2str(examIDcurrent) ' (' folderFull ') because it is a duplicate of ' entriesFound{jEntry}.location])
                            db.remove(examIDcurrent);
                            removedExamIDCounter = removedExamIDCounter + 1;
                            removedExamID{removedExamIDCounter}.exID = examIDcurrent;
                            removedExamID{removedExamIDCounter}.exName = entryInCurrentFolder.examName;
                            removedExamID{removedExamIDCounter}.SUID = animalID;
                            removedExamID{removedExamIDCounter}.raw = entryInCurrentFolder.location;
                        end
                        
                    end
                end
            end
        end
    end
end

%% save database changes
db.save;

for jCount = 1:size(removedExamID,2)
    deletedFolders{jCount,1} = ['SU' removedExamID{jCount}.SUID '/EX' num2str(removedExamID{jCount}.exID,'%06d') '_' strrep(removedExamID{jCount}.exName,' ','') '/'];
    deletedFolders{jCount,2} = removedExamID{jCount}.raw;
end

tableOut = cell2table(deletedFolders)
writetable(tableOut,fullfile('/shared/studies/nonregulated/Bruker_studies/db/mainanance_scripts/deletedEntries.csv'))


%% move data that is not in db out of data folder
targetFolderNotInDb = fullfile(dataStoragePath,'maintanance_dataNotInDB/');
mkdir(targetFolderNotInDb)
for jDataset = 1:size(notInDb,2)
    shellStr = ['sudo mv ' fullfile(dataStoragePath,notInDb{1,jDataset}) ' ' targetFolderNotInDb];
    %disp(shellStr)
    exec_system_command(shellStr);
end